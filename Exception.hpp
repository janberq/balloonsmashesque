#ifndef _EXCEPTION_HPP_INCLUDED
#define _EXCEPTION_HPP_INCLUDED

#include <string>
#include <exception>

namespace BalloonSmashEsque
{

class TException : public std::exception
{

public:

    TException();
    TException(const std::string &msg);
    const char *what() const noexcept;

private:

    std::string msg_;

};

}

#endif // _EXCEPTION_HPP_INCLUDED
