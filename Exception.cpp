#include "Exception.hpp"

namespace BalloonSmashEsque
{

TException::TException()
{
}

TException::TException(const std::string& msg)
{
    msg_ = msg;
}

const char * TException::what() const noexcept
{
    return msg_.c_str();
}

}
