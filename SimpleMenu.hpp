#ifndef _SIMPLE_MENU_HPP
#define _SIMPLE_MENU_HPP

#include <SDL2pp/SDL2pp.hh>
#include <functional>
#include <memory>
#include <vector>
#include "Component.hpp"

namespace BalloonSmashEsque
{

class TSimpleMenu : public TComponent
{

public:

    TSimpleMenu(
        std::shared_ptr<SDL2pp::Renderer> renderer,
        std::shared_ptr<SDL2pp::Font> font,
        std::shared_ptr<SDL2pp::Texture> buttonnormaltexture,
        std::shared_ptr<SDL2pp::Texture> buttonhovertexture,
        std::shared_ptr<SDL2pp::Texture> buttonpressedtexture,
        const SDL_Color &foreground,
        const SDL2pp::Point &menupos,
        int buttonseparation = 2,
        int imageleftrightmargin = 0);

    enum EImageAlign
    {
        IMAGE_ALIGN_LEFT,
        IMAGE_ALIGN_RIGHT,
        IMAGE_ALIGN_CENTER
    };

    TSimpleMenu &operator()(
        std::size_t id,
        const std::string &text,
        std::shared_ptr<SDL2pp::Texture> buttonimage = nullptr,
        EImageAlign imagealign = IMAGE_ALIGN_LEFT
    );

    void OnClick(std::function<void (std::size_t)> onclick);

    int GetWidth() const override;
    int GetHeight() const override;

    void Render() override;
    void Handle(SDL_Event &e) override;

private:

    struct button_t {
        std::size_t id;
        std::shared_ptr<SDL2pp::Texture> normal;
        std::shared_ptr<SDL2pp::Texture> hover;
        std::shared_ptr<SDL2pp::Texture> pressed;
    };

    std::vector<button_t> buttons_;

    std::vector<button_t>::const_iterator hovered_;
    std::vector<button_t>::const_iterator pressed_;

    std::shared_ptr<SDL2pp::Renderer> render_;
    std::shared_ptr<SDL2pp::Texture> buttonnormal_;
    std::shared_ptr<SDL2pp::Texture> buttonhover_;
    std::shared_ptr<SDL2pp::Texture> buttonpressed_;
    std::shared_ptr<SDL2pp::Font> font_;

    SDL_Color fg_;

    int buttonseparation_;
    int imageleftrightmargin_;
    int buttonwidth_;
    int buttonheight_;

    std::function<void (std::size_t)> onclick_;

};

}

#endif
