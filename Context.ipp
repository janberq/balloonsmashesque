// IMPLEMENTATION FOR TEMPLATE FUNCTIONS

template <typename T>
T TContext::Get(const std::string &name) const
{
    std::string _name;
    {
        auto it = aliases_.find(name);
        if (it != aliases_.end())
        {
            _name = it->second;
        }
        else
        {
            _name = name;
        }
    }
    auto it = objects_.find(_name);
    if (it != objects_.end())
    {
        return boost::any_cast<T>(it->second.obj); // may throw
    }
    else
    {
        throw std::runtime_error("error: not found such object: " + _name);
    }
}

template <typename T>
void TContext::Set(const std::string &name, const T &value)
{
    std::string _name;
    {
        auto it = aliases_.find(name);
        if (it != aliases_.end())
        {
            _name = it->second;
        }
        else
        {
            _name = name;
        }
    }
    auto &o = objects_[_name];
    o.obj = value;
    for (auto &f : o.notif)
    {
        if (f) f();
    }
}


template <typename T>
void TContext::AddNotification(const std::string &name, TContext::notiffunc_t<T> f)
{
    std::string _name;
    {
        auto it = aliases_.find(name);
        if (it != aliases_.end())
        {
            _name = it->second;
        }
        else
        {
            _name = name;
        }
    }
    auto it = objects_.find(_name);
    if (it != objects_.end())
    {
        it->second.notif.push_back([this, f, _name]() { f(Get<T>(_name)); });
    }
    else
    {
        throw std::runtime_error("error: object with such name doesn't exist: " + _name);
    }
}

template <typename T>
std::shared_ptr<T> TContext::GetObj(const std::string &name)
{
    std::string _name;
    {
        auto it = aliases_.find(name);
        if (it != aliases_.end())
        {
            _name = it->second;
        }
        else
        {
            _name = name;
        }
    }
    auto it = objects_.find(_name);
    if (it != objects_.end())
    {
        return boost::any_cast<std::shared_ptr<T>>(it->second.obj); // may throw
    }
    else
    {
        throw std::runtime_error("error: not found such object: " + name);
    }
}

template <typename T>
void TContext::AddObj(const std::string &name, std::shared_ptr<T> obj)
{
    auto it = objects_.find(name);
    if (it == objects_.end())
    {
        objects_[name].obj = obj;
    }
    else
    {
        throw std::runtime_error("error: object with such name already exists: " + name);
    }
}


