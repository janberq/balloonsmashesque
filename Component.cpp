#include "Component.hpp"

namespace BalloonSmashEsque
{

int TComponent::GetWidth() const
{
    return 0;
}

int TComponent::GetHeight() const
{
    return 0;
}

SDL2pp::Point TComponent::GetSize() const
{
    return { GetWidth(), GetHeight() };
}

int TComponent::GetX() const
{
    return pos_.x;
}

int TComponent::GetY() const
{
    return pos_.y;
}

SDL2pp::Point TComponent::GetPosition() const
{
    return pos_;
}

void TComponent::SetPosition(int x, int y)
{
    SetPosition({x, y});
}

void TComponent::SetPosition(const SDL2pp::Point& pos)
{
    pos_ = pos;
}

void TComponent::Render()
{
    
}

void TComponent::Handle(SDL_Event& e)
{
    
}

TComponent::~TComponent()
{
    
}

}

