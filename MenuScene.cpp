#include "MenuScene.hpp"
#include "Game.hpp"
#include "Exception.hpp"

#include <sstream>
#include <iomanip>

namespace BalloonSmashEsque
{

TMenuScene::TMenuScene(TGame *game)
{
    if (game == nullptr) throw TException("game == null!!!");
    game_ = game;
    renderer_ = game_->GetRenderer();
    win_ = game_->GetWindow();

    current_ = false;
    paused_ = false;

    // load logo

    auto logo_txt = game_->GetObj<SDL2pp::Texture>("texture.logo");
    logo_ = std::make_shared<TStaticImage>(
        renderer_,
        logo_txt);

    // load menu
    menu_ = std::make_shared<TSimpleMenu>(
        *(game_->GetObj<TSimpleMenu>("menubase"))); // clone the base

    (*menu_)
        (0, "PLAY", game_->GetObj<SDL2pp::Texture>("texture.icons.play"))
        (1, "OPTS", game_->GetObj<SDL2pp::Texture>("texture.icons.options"))
        (2, "QUIT", game_->GetObj<SDL2pp::Texture>("texture.icons.exit"));

    menu_->OnClick([this](std::size_t n) {
        switch (n)
        {
            case 0:
                game_->SetCurrentScene("mainScene");
                break;
            case 1:
                break;
            case 2:
                game_->Stop();
                break;
        };
    });

    // load the layout
    boxer_ = game_->GetObj<TBoxer>("boxer.menuscene");
    boxer_->w = win_->GetWidth();
    boxer_->h = win_->GetHeight();
    boxer_->BindComponent("logo", logo_.get());
    boxer_->BindComponent("menu", menu_.get());
    boxer_->Prepare();
    boxer_->DoBoxing();

    // load background transformation
    bgtrans_ = game_->GetObj<TBackgroundTransition>("bgtrans");
}

TMenuScene::~TMenuScene()
{
}

void TMenuScene::Render()
{
    // draw background
    bgtrans_->Render();

    // draw logo
    logo_->Render();

    // draw menu
    menu_->Render();
}

void TMenuScene::Handle(SDL_Event &e)
{
    if (e.type == SDL_QUIT)
    {
        game_->Stop();
        return ;
    }

    menu_->Handle(e);
}

void TMenuScene::OnPause()
{
    paused_ = true;
}

void TMenuScene::OnResume()
{
    paused_ = false;
}

void TMenuScene::OnCurrent()
{
    current_ = true;
}

void TMenuScene::OnNotCurrent()
{
    current_ = false;
}

}
