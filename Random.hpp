// canberk.sonmez.409@gmail.com
// 15 jan 2016

#ifndef _RANDOM_HPP_INCLUDED
#define _RANDOM_HPP_INCLUDED

#include <random>

namespace BalloonSmashEsque
{

class TRandomGenerator {

public:

  TRandomGenerator();

  TRandomGenerator(const TRandomGenerator &) = delete;
  TRandomGenerator &operator=(const TRandomGenerator &) = delete;

  TRandomGenerator(TRandomGenerator &&);
  TRandomGenerator &operator=(TRandomGenerator &&);

  ~TRandomGenerator();

  double NewDouble(double min, double max);
  long NewLong(long min, long max);

private:

  std::mt19937 gen_;

};

}

#endif // _RANDOM_HPP_INCLUDED

