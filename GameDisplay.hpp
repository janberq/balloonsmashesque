#ifndef _GAMEDISPLAY_HPP_INCLUDED
#define _GAMEDISPLAY_HPP_INCLUDED

#include <SDL2pp/SDL2pp.hh>
#include <memory>
#include <vector>
#include <list>
#include <set>

#include "Point.hpp"
#include "Component.hpp"

namespace BalloonSmashEsque
{

class TGameDisplay : public TComponent
{

public:

    struct ball_t // or balloon if you want :/
    {
        TPoint<int> coords; // in hex coords
        std::size_t color;
    };

    struct bomb_t
    {
        TPoint<double> pos;
        TPoint<double> velocity; // px/sec
    };

public:

    TGameDisplay(
        std::shared_ptr<SDL2pp::Renderer> renderer,
        const std::vector<std::shared_ptr<SDL2pp::Texture>> &balltextures,
        std::shared_ptr<SDL2pp::Texture> bombtexture,
        std::shared_ptr<SDL2pp::Texture> blocktexture,
        const SDL2pp::Point &pos,
        const SDL2pp::Point &sz,
        double hexgridrad,
        const SDL_Color &borderclr = SDL_Color{ 0, 0, 0, 255});

    void Handle(SDL_Event &e);
    void Render();

    void SetBalls(const std::list<ball_t> &balls);
    void SetBombs(const std::list<bomb_t> &bombs);

    void AddBall(const ball_t &ball);
    void AddBomb(const bomb_t &bomb);

    void SetBlockPosition(double pos);

    double GetBlockMaxPos() const;
    double GetBlockMinPos() const;

    double GetBlockWidth() const;
    SDL2pp::Point GetBlockSize() const;

    void SetWidth(int wi);
    void SetHeight(int he);

    void SetSize(const SDL2pp::Point &sz);

    int GetWidth() const override;
    int GetHeight() const override;

    bool IsKeyboardEnabled() const;
    void SetKeyboardEnable(bool v = true);

    // in px/sec
    double GetKeyboardSpeed() const;
    void SetKeyboardSpeed(double speed);

    void OnBallsGone(std::function<void ()> f);
    void OnAllBallsGone(std::function<void ()> f);
    void OnBombFallen(std::function<void (const bomb_t&)> f);

    void ResetTimer();
    void NextStep();

    TPoint<double> ConvHexToCart(const TPoint<int> &hex) const;
    SDL2pp::Point ConvCartToScr(const TPoint<double> &cart) const;

private:

    std::shared_ptr<SDL2pp::Renderer> renderer_;

    std::vector<std::shared_ptr<SDL2pp::Texture>> balltextures_;
    std::shared_ptr<SDL2pp::Texture> bombtexture_;
    std::shared_ptr<SDL2pp::Texture> blocktexture_;

    double hexgridrad_;
    double ballrad_;
    double bombrad_;

    std::list<ball_t> balls_;
    std::list<bomb_t> bombs_;

    double blockcoord_;
    double blockhe_;
    double blockwi_;

    SDL2pp::Point sz_;

    SDL_Color borderclr_;

    // pos in cart
    void RenderCentered(
        std::shared_ptr<SDL2pp::Texture> texture,
        const TPoint<double> &pos);

    void RenderBombs();
    void RenderBalls();
    void RenderBlock();
    void RenderBorder();

    void Motion();
    void HandleCollisions();

    void BreakBallsAt(const TPoint<int> &pos);

    std::function<void ()> onballsgone_;
    std::function<void ()> onallballsgone_;
    std::function<void (const bomb_t&)> onbombfallen_;

    std::size_t tprevious_;

    bool keyboardenabled_;
    double keyboardspeed_;

    struct key_t
    {
        SDL_Keycode code;
        mutable std::size_t t;
        constexpr bool operator<(const key_t &other) const
        { return code < other.code; }
    };
    std::set<key_t> pressedkeys_;
    std::set<key_t>::iterator GetKey(SDL_Keycode code);
};

}

#endif // _GAMEDISPLAY_HPP_INCLUDED
