#include "Game.hpp"

#include "MenuScene.hpp"
#include "MainScene.hpp"

#include "BackgroundTransition.hpp"
#include "SimpleMenu.hpp"
#include "Button.hpp"

#include "Server.hpp"

#include "Boxer.hpp"
#include "RegisterSDL2pp.hpp"

#include <iostream>
#include <fstream>
#include <boost/program_options.hpp>

#include "PauseDialog.hpp"

namespace BalloonSmashEsque
{

void TGame::Init(int argc, char **argv)
{
    /* Initials */

    datadir_ = "../share/balloonsmashesque-data";
    short portnum = 1025;

    /* Parse command line */

    namespace po = boost::program_options;

    po::options_description desc("Options");
    desc.add_options()
        ("help", "print help")
        ("datadir", po::value<std::string>(&datadir_), "sets the data directory")
        ("portnum", po::value<short>(&portnum), "sets the port number to listen");

    po::variables_map vm;

    // no need for try, errors should be caught in main
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);

        if (vm.count("help"))
        {
            norun_ = true;
            std::cout << "BalloonSmashEsque command line:" << std::endl <<
                desc << std::endl;
            return ;
        }

        po::notify(vm);
    }

    Set<std::string>("config.str.datadir", datadir_);

    /* load resources */
    std::ifstream ifs(GetResourcePath("resources.xml"));
    ExecuteXMLData(ifs); /* load resources */

    ExecuteHooks("config.afterCmdArgsParsed");

    /* Create Window and Renderer */

    window_ = std::make_shared<SDL2pp::Window>(
        "BalloonSmashEsque",
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        1280, 960, SDL_WINDOW_OPENGL);
    renderer_ = std::make_shared<SDL2pp::Renderer>(
        *window_,
        -1,
        SDL_RENDERER_ACCELERATED);

    AddObj<SDL2pp::Window>("game.window", window_);
    AddObj<SDL2pp::Renderer>("game.renderer", renderer_);

    RegisterSDL2ppTextureToContext(*this);
    RegisterSDL2ppFontToContext(*this);
    RegisterSDLColorToContext(*this);
    RegisterBoxerToContext(*this);

    ExecuteHooks("game.afterSDLInit");

    /* initialize background transition */
    /* TODO: WRITE AN XML LOADER FUNCTION FOR THIS */
    {
        auto bgtrans = std::make_shared<TBackgroundTransition>(renderer_,
            SDL2pp::Rect(0, 0, window_->GetWidth(), window_->GetHeight()));

        auto func1 = TBackgroundTransition::linearTransition(1000, 500, 500, 0.1, 500, 500, 0.1);
        auto func2 = TBackgroundTransition::logarithmicTransition(2000);

        int backgroundsCount = Get<int>("int.backgroundsCount");
        int j = 1;

        for (int j = 1; j <= backgroundsCount; ++j)
        {
            (*bgtrans)(
                GetObj<SDL2pp::Texture>(
                    "texture.backgrounds." + std::to_string(j)),
                10000,
                (j % 2) ? func1 : func2);
        }

        bgtrans->Start();

        AddObj("bgtrans", bgtrans);
    }

    /* initialize menu */
    /* TODO: WRITE AN XML LOADER FOR THIS */
    {

        auto menu_base = std::make_shared<TSimpleMenu>(
            renderer_,
            GetObj<SDL2pp::Font>("font.default"),
            GetObj<SDL2pp::Texture>("texture.button.normal"),
            GetObj<SDL2pp::Texture>("texture.button.hover"),
            GetObj<SDL2pp::Texture>("texture.button.pressed"),
            SDL_Color { 80, 45, 42, 255 }, /* TODO: WRITE A COLOR LOADER,
                                            * LOAD FROM XML */
            SDL2pp::Point { 0, 0 },
            2,
            18
        );

        AddObj("menubase", menu_base);
    }

    /* load button factory */
    {

        auto buttonFactory = std::make_shared<TButtonFactory>(
            renderer_,
            GetObj<SDL2pp::Texture>("texture.buttonsmall.normal"),
            GetObj<SDL2pp::Texture>("texture.buttonsmall.hover"),
            GetObj<SDL2pp::Texture>("texture.buttonsmall.pressed"));

        AddObj("smallButtonFactory", buttonFactory);
        // TODO FIX WHEN BUTTONFACTORY ADDED TO XML
        CreateAlias("pauseDialog.buttonfactory.default", "smallButtonFactory");

    }

    /* Initialize server */
    {
        /* first register our event type */
        Uint32 data_recv_event = SDL_RegisterEvents(1);
        Uint32 log_event = SDL_RegisterEvents(1);
        Set<Uint32>("sdl_event.DataRecv", data_recv_event);
        Set<Uint32>("sdl_event.LogEvent", log_event);

        std::shared_ptr<TServer> srv = std::make_shared<TServer>(portnum);
        srv->OnLog([log_event](const std::string &log)
        {
#if 0
            SDL_Event e;
            SDL_zero(e);
            e.type = log_event;
            e.user.code = 0;
            std::string *str = new std::string(log);
            e.user.data1 = str;
            SDL_PushEvent(&e);
#endif
            std::cout << "Server: " << log << std::endl;
        });
        srv->OnDataRecv([data_recv_event](const std::string &data)
        {
            SDL_Event e;
            SDL_zero(e);
            e.type = data_recv_event;
            e.user.code = 0;
            int &intdata = *(int *)(e.user.data1 = new int);
            intdata = std::atoi(data.c_str());
            SDL_PushEvent(&e);
        });
        srv->Start();
        AddObj("game.server", srv);
    }

    /* some properties */
    Set<bool>("muted", false);

    /* Add scenes */
    AddScene("menuScene", new TMenuScene(this));
    SetCurrentScene("menuScene");

    AddScene("mainScene", new TMainScene(this));

    /* Add dialog */
    auto pauseDialog = std::make_shared<TPauseDialog>(this);

    Set<bool>("game.paused", false);

    AddNotification<bool>("game.paused", [this, pauseDialog](const bool &paused)
    {
        if (paused)
        {
            SetCurrentDialog(pauseDialog.get());
        }
        else
        {
            SetCurrentDialog(nullptr);
        }
    });
}

}
