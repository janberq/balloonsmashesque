#include "BackgroundTransition.hpp"
#include "Exception.hpp"

#include <cmath>

namespace BalloonSmashEsque
{

TBackgroundTransition::TBackgroundTransition(
    std::shared_ptr<SDL2pp::Renderer> renderer,
    const SDL2pp::Rect &dest)
{
    if (!renderer) throw TException("Null renderer!");
    renderer_ = renderer;
    dest_ = dest;
}

TBackgroundTransition &TBackgroundTransition::operator()(
    std::shared_ptr<SDL2pp::Texture> texture,
    size_t duration,
    TTransitionFunction transition_function)
{
    if (!texture) throw TException("Null texture!");
    if (!transition_function) throw TException("Bad transition function!");
    durations_.push_back(duration);
    textures_.push_back(texture);
    transition_functions_.push_back(transition_function);
    sizes_.push_back(texture->GetSize());
    return *this;
}

void TBackgroundTransition::Start(std::size_t start_texture)
{
    transition_ = false;
    current_ = start_texture;
    time_ = SDL_GetTicks();
}

void TBackgroundTransition::Render()
{
    if (textures_.empty())
        return ;
    std::size_t now = SDL_GetTicks();
    if (!transition_)
    {
        renderer_->Copy(*textures_[current_], SDL2pp::NullOpt, dest_);
        if (now - time_ >= durations_[current_])
        {
            transition_ = true; // start transition
            time_ = now;
        }
    }
    else // now, transition!
    {
        // first apply transition function
        transition_ = !transition_functions_[(current_ + 1) % textures_.size()](
            now - time_, proportion_);
        std::size_t a, b;
        double p;
        if (proportion_ >= 0)
        {
            std::size_t n = (std::size_t) proportion_;
            a = (current_ + n) % (textures_.size());
            b = (current_ + n + 1) % (textures_.size());
            p = std::fmod(proportion_, 1);
        }
        else
        {
            std::size_t n = (std::size_t)(-proportion_);
            long a_old = (current_ + n - 1);
            long b_old = (current_ + n);
            while (a_old < 0) a_old += textures_.size();
            while (b_old < 0) b_old += textures_.size();
            a = a_old;
            b = b_old;
            p = 1+std::fmod(proportion_, 1);  // for proportion = -1.7
                                                     // it's 1 + (-0.7)
        }
        SDL2pp::Rect src, dest;
        // draw (1-p) right part of a
        src.x = sizes_[a].x * p;
        src.y = 0;
        src.w = sizes_[a].x * (1.0-p);
        src.h = sizes_[a].y;
        dest.x = dest_.x;
        dest.y = dest_.y;
        dest.w = dest_.w * (1.0-p);
        dest.h = dest_.h;
        renderer_->Copy(*textures_[a], src, dest);
        // draw    p  left  part of b
        src.x = 0;
        src.y = 0;
        src.w = sizes_[b].x * p;
        src.h = sizes_[b].y;
        dest.x = dest_.x + dest_.w * (1.0-p);
        dest.y = dest_.y;
        dest.w = dest_.w * p;
        dest.y = dest_.y;
        renderer_->Copy(*textures_[b], src, dest);
        if (!transition_)
        {
            current_++;
            current_ %= textures_.size();
            time_ = now; /* start normally */
        }
    }
}

/* some transitions */

TBackgroundTransition::TTransitionFunction
TBackgroundTransition::linearTransition(
    std::size_t maintransitionduration,
    std::size_t previousdurationfirst,
    std::size_t previousdurationsecond,
    double previousproportion,
    std::size_t nextdurationfirst,
    std::size_t nextdurationsecond,
    double nextproportion)
{
    std::size_t t1 = previousdurationfirst;
    std::size_t t2 = t1 + previousdurationsecond;
    std::size_t t3 = t2 + maintransitionduration;
    std::size_t t4 = t3 + nextdurationfirst;
    std::size_t t5 = t4 + nextdurationsecond;
    return [=](int t, double &prop) -> bool
    {
        if (t < t1) // the first transition phase
        {
            prop = -previousproportion * ((double)t) / previousdurationfirst;
        }
        else if (t < t2) // second phase
        {
            prop = previousproportion * ((double)(t-t1) /
                previousdurationsecond - 1);
        }
        else if (t < t3)
        {
            prop = ((double)(t-t2)) / (maintransitionduration);
        }
        else if (t < t4)
        {
            prop = 1 + ((double)(t-t3)) * nextproportion / nextdurationfirst;
        }
        else if (t < t5)
        {
            // rule learned:
            // do not subtract unsigned values as they result negatives
            prop = 1 - ((double)t-(double)t5) * nextproportion / nextdurationsecond;
        }
        else
        {
            return true; // transition is over
        }
        return false; // transition is not over
    };
}

TBackgroundTransition::TTransitionFunction
TBackgroundTransition::logarithmicTransition(
    std::size_t maintransitionduration)
{
    return [=](int t, double &prop) -> bool
    {
        if (t < maintransitionduration)
        {
            prop = std::log(1+t) / std::log(1 + maintransitionduration);
        }
        else
        {
            return true;
        }
        return false;
    };
    
}

}
