#ifndef _REGISTERSDL2PP_HPP_INCLUDED
#define _REGISTERSDL2PP_HPP_INCLUDED

namespace BalloonSmashEsque
{

class TContext;

void RegisterSDL2ppTextureToContext(TContext &ctx);
void RegisterSDL2ppFontToContext(TContext &ctx);
void RegisterSDLColorToContext(TContext &ctx);

}

#endif // _REGISTERSDL2PP_HPP_INCLUDED
