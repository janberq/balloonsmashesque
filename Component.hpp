#ifndef _COMPONENT_HPP_INCLUDED
#define _COMPONENT_HPP_INCLUDED

#include <SDL2pp/Point.hh>
#include <SDL_events.h>

namespace BalloonSmashEsque
{

class TComponent;

class TComponent
{

public:

    virtual int GetWidth() const;
    virtual int GetHeight() const;

    SDL2pp::Point GetSize() const;

    int GetX() const;
    int GetY() const;

    virtual SDL2pp::Point GetPosition() const;

    void SetPosition(int x, int y);
    virtual void SetPosition(const SDL2pp::Point &pos);

    virtual void Render();
    virtual void Handle(SDL_Event &e);

    virtual ~TComponent();

protected:

    SDL2pp::Point pos_;

};

}

#endif // _COMPONENT_HPP_INCLUDED
