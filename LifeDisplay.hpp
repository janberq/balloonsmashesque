#ifndef _LIFEDISPLAY_HPP_INCLUDED
#define _LIFEDISPLAY_HPP_INCLUDED

#include <SDL2pp/SDL2pp.hh>
#include <memory>

#include "Component.hpp"

namespace BalloonSmashEsque
{

class TLifeDisplay : public TComponent
{

public:

    TLifeDisplay(
        std::shared_ptr<SDL2pp::Renderer> renderer,
        std::shared_ptr<SDL2pp::Font> font,
        std::shared_ptr<SDL2pp::Texture> hearttexture,
        const SDL2pp::Point &pos = { 0, 0 },
        int heartseparation = 0,
        int textheartseparation = 0
    );

    int GetLifeCount() const;
    void SetLifeCount(int n);

    void DieOneMore();
    void DieFull();

    void Render() override;

    int GetWidth() const override;
    int GetHeight() const override;

private:

    std::shared_ptr<SDL2pp::Renderer> renderer_;
    std::shared_ptr<SDL2pp::Texture> text_;
    std::shared_ptr<SDL2pp::Texture> heart_;

    int heartseparation_;
    int textheartseparation_;

    int lifecount_;

};

}

#endif // _LIFEDISPLAY_HPP_INCLUDED
