// canberk.sonmez.409@gmail.com
// 15 jan 2016

#include <chrono>
#include <string>
#include "Random.hpp"

namespace BalloonSmashEsque
{

TRandomGenerator::TRandomGenerator() :
  gen_{(unsigned)std::chrono::system_clock::now().time_since_epoch().count()} {  }

TRandomGenerator::TRandomGenerator(TRandomGenerator &&) = default;

TRandomGenerator &TRandomGenerator::operator=(TRandomGenerator &&) = default;

TRandomGenerator::~TRandomGenerator() = default;

double TRandomGenerator::NewDouble(double min, double max) {
  std::uniform_real_distribution<double> dist(min, max);
  return dist(gen_);
}

long TRandomGenerator::NewLong(long min, long max) {
  std::uniform_int_distribution<long> dist(min, max);
  return dist(gen_);
}

}
