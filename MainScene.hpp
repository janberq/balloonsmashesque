#ifndef _MAINSCENE_HPP_INCLUDED
#define _MAINSCENE_HPP_INCLUDED

#include "Scene.hpp"
#include <SDL2pp/SDL2pp.hh>
#include <memory>

namespace BalloonSmashEsque
{

class TBackgroundTransition;
class TSimpleMenu;
class TButton;
class TLifeDisplay;
class TGameDisplay;
class TBoxer;
class TStaticText;

class TMainScene : public TScene
{

public:

    TMainScene(TGame *game);

    TMainScene(const TMainScene &) = delete;
    TMainScene &operator=(const TMainScene &) = delete;

    TMainScene(TMainScene &&) = delete;
    TMainScene &operator=(TMainScene &&) = delete;

    virtual ~TMainScene();

    // Handling and Rendering

    void Render() override;
    void Handle(SDL_Event &e) override;

    void OnPause() override;
    void OnResume() override;

    void OnCurrent() override;
    void OnNotCurrent() override;

private:

    TGame *game_;
    bool paused_;
    bool current_;

    bool muted_;

    std::shared_ptr<SDL2pp::Renderer> render_;
    std::shared_ptr<SDL2pp::Window> win_;

    std::shared_ptr<TBackgroundTransition> bgtrans_;
    std::shared_ptr<TSimpleMenu> menu_;
    std::shared_ptr<TGameDisplay> gdisp_;

    std::shared_ptr<TButton> mute_;
    std::shared_ptr<TButton> unmute_;
    std::shared_ptr<TButton> pause_;
    std::shared_ptr<TButton> play_;

    std::shared_ptr<TStaticText> status_;

    std::shared_ptr<TLifeDisplay> lifedisp_;

    std::shared_ptr<TBoxer> boxer_;

    Uint32 dataRecvEvent_;

    int wi_, he_;

};

}

#endif // _MAINSCENE_HPP_INCLUDED
