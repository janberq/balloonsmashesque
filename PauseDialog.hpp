#ifndef PAUSEDIALOG_HPP_INCLUDED
#define PAUSEDIALOG_HPP_INCLUDED

#include <SDL2pp/Renderer.hh>
#include <SDL2pp/Window.hh>
#include <string>
#include "Button.hpp"
#include "Boxer.hpp"
#include "Game.hpp"
#include "Dialog.hpp"
#include "StaticComponents.hpp"

namespace BalloonSmashEsque
{

class TPauseDialog : public TDialog
{

public:

    // game should provide:
    //    pauseDialog.boxer.default
    //      components: playButton, pauseText
    //    pauseDialog.font.default
    //    pauseDialog.texture.play
    //    pauseDialog.color.shade
    //    pauseDialog.color.pauseText
    //    pauseDialog.string.pause
    //    pauseDialog.buttonfactory.default
    TPauseDialog(TGame *game);

    void Render() override;
    void Handle(SDL_Event & e) override;

    SDL_Color GetShadeColor() const override;

    virtual ~TPauseDialog();

private:

    std::shared_ptr<TStaticText> pause_text_;
    std::shared_ptr<TButton> play_button_;
    std::shared_ptr<TBoxer> boxer_;

    TGame *game_;
    std::shared_ptr<SDL2pp::Window> window_;
    std::shared_ptr<SDL2pp::Renderer> renderer_;

    SDL_Color shade_color_;

};

}

#endif // PAUSEDIALOG_HPP_INCLUDED
