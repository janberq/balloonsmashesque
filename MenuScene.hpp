#ifndef _MENUSCENE_HPP_INCLUDED
#define _MENUSCENE_HPP_INCLUDED

#include "Scene.hpp"
#include "BackgroundTransition.hpp"
#include "SimpleMenu.hpp"
#include "StaticComponents.hpp"
#include "Boxer.hpp"

#include <SDL2pp/SDL2pp.hh>
#include <memory>
#include <vector>

namespace BalloonSmashEsque
{

class TGame;

class TMenuScene : public TScene
{
public:

    // Constructors and destructors

    TMenuScene(TGame *game);

    TMenuScene(const TMenuScene &) = delete;
    TMenuScene &operator=(const TMenuScene &) = delete;

    TMenuScene(TMenuScene &&) = delete;
    TMenuScene &operator=(TMenuScene &&) = delete;

    virtual ~TMenuScene();

    // Handling and Rendering

    void Render() override;
    void Handle(SDL_Event &e) override;

    void OnPause() override;
    void OnResume() override;

    void OnCurrent() override;
    void OnNotCurrent() override;

private:

    TGame *game_;
    std::shared_ptr<SDL2pp::Renderer> renderer_;
    std::shared_ptr<SDL2pp::Window> win_;

    bool current_;
    bool paused_;

    std::shared_ptr<TBackgroundTransition> bgtrans_;
    std::shared_ptr<TSimpleMenu> menu_;
    std::shared_ptr<TStaticImage> logo_;
    std::shared_ptr<TBoxer> boxer_;

};

}

#endif // _MENUSCENE_HPP_INCLUDED
