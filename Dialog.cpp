#include "Dialog.hpp"

SDL_Color BalloonSmashEsque::TDialog::GetShadeColor() const
{
    return {0, 0, 0, 50};
}

void BalloonSmashEsque::TDialog::Handle(SDL_Event& e)
{
}

void BalloonSmashEsque::TDialog::OnGone()
{
}

void BalloonSmashEsque::TDialog::OnShown()
{
}

void BalloonSmashEsque::TDialog::Render()
{
}

BalloonSmashEsque::TDialog::~TDialog()
{
}
