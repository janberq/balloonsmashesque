#include "Server.hpp"

#include <boost/asio.hpp>
#include <istream>
#include <utility>
#include <thread>
#include <mutex>

namespace BalloonSmashEsque
{

using namespace boost::asio;
using namespace std;
using boost::system::error_code;

// TServerImpl Impl

class TServerImpl
{
public:

    TServerImpl(unsigned short portnum, bool autorelisten);

    void OnDataRecv(std::function<void (const std::string &)> func);
    void OnLog(std::function<void (const std::string &)> func);

    void Start();

    bool IsListening() const;
    bool IsConnected() const;

    ~TServerImpl();

private:
    io_service io_service_;
    unique_ptr<io_service::work> work_;
    ip::tcp::acceptor acceptor_;
    ip::tcp::socket socket_;
    boost::asio::streambuf streambuf_;
    thread thread_;
    bool islistening_;
    bool isconnected_;
    bool autorelisten_;
    bool isstarted_;
    std::function<void (const std::string &)> ondatarecv_;
    std::function<void (const std::string &)> onlog_;

    void DoListen();
    void DoRead();
    void DoLog(const std::string &log);
};

// TServerImpl Impl

TServerImpl::TServerImpl(unsigned short portnum, bool autorelisten) :
    work_(new io_service::work(io_service_)),
    acceptor_(io_service_, ip::tcp::endpoint(ip::tcp::v4(), portnum)),
    socket_(io_service_),
    islistening_(false),
    isconnected_(false),
    autorelisten_(autorelisten)
{

}

void TServerImpl::OnDataRecv(function<void (const std::string &)> func)
{
    ondatarecv_ = move(func);
}

void TServerImpl::OnLog(function<void (const std::string &)> func)
{
    onlog_ = move(func);
}

void TServerImpl::Start()
{
    if (!isstarted_)
    {
        DoListen();
        thread_ = thread([this]() { io_service_.run(); });
        isstarted_ = true;
    }
}

bool TServerImpl::IsListening() const
{
    return islistening_;
}

bool TServerImpl::IsConnected() const
{
    return isconnected_;
}

TServerImpl::~TServerImpl()
{
    work_.reset();
    try { acceptor_.close(); } catch (...) {  }
    try { io_service_.reset(); } catch (...) {  }
    try { socket_.close(); } catch (...) {  }
    try { thread_.join(); } catch (...) {  }
}

void TServerImpl::DoListen()
{
    acceptor_.async_accept(socket_, [this](boost::system::error_code ec)
    {
        islistening_ = false;
        DoLog("Not listening anymore.");
        if (!ec)
        {
            DoLog("Socket successfully accepted.");
            DoLog("Connected.");
            DoRead();
            
        }
        else
        {
            DoLog("Listening error: " + ec.message() + " .");
        }
    });
    islistening_ = true;
    DoLog("Listening.");
}

void TServerImpl::DoLog(const string& log)
{
    if (onlog_) onlog_(log);
}

void TServerImpl::DoRead()
{
    async_read_until(socket_, streambuf_, '\n', [this](boost::system::error_code ec, size_t len)
    {
        if (!ec)
        {
            std::string in;
            std::istream is(&streambuf_);
            std::getline(is, in, '\n');
            if (ondatarecv_) ondatarecv_(in);
            DoRead();
        }
        else
        {
            DoLog("Read error: " + ec.message() + " .");
            try { socket_.close(); } catch (...) {  }
            socket_ = ip::tcp::socket(io_service_);
            DoLog("Disconnected.");
            if (autorelisten_) DoListen();
        }
    });
}

// TServer Impl

TServer::TServer(unsigned short portnum, bool autorelisten)
{
    impl_.reset(new TServerImpl(portnum, autorelisten));
}

TServer::TServer(TServer&& other)
{
    *this = move(other);
}

TServer& TServer::operator=(TServer&& other)
{
    impl_.swap(other.impl_);
    return *this;
}

void TServer::OnDataRecv(std::function<void (const std::string &)> func)
{
    impl_->OnDataRecv(move(func));
}

void TServer::Start()
{
    impl_->Start();
}

void TServer::OnLog(std::function<void (const std::string &)> func)
{
    impl_->OnLog(move(func));
}

bool TServer::IsListening() const
{
    return impl_->IsListening();
}

bool TServer::IsConnected() const
{
    return impl_->IsConnected();
}

bool TServer::IsOK() const
{
    return impl_.get() != nullptr;
}

TServer::~TServer() = default;

}
