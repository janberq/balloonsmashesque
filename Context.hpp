#ifndef _CONTEXT_HPP_INCLUDED
#define _CONTEXT_HPP_INCLUDED

#include <istream>
#include <sstream>
#include <string>
#include <memory>
#include <functional>
#include <map>
#include <list>
#include <algorithm>
#include <stdexcept>
#include <selene.h>
#include <pugixml.hpp>
#include <boost/any.hpp>

namespace BalloonSmashEsque
{

class TContext
{

public:

    using factoryfunc_t = std::function<void (TContext &, pugi::xml_node)>;

    template <typename T>
    using notiffunc_t = std::function<void (const T&)>;

    using grouphandler_t = std::function<bool (const std::string &)>;

    using hookfunc_t = std::function<void ()>;

public:

    TContext();

    bool EvaluateLuaBool(const std::string &s);
    void ExecuteLua(const std::string &s);

    void ExecuteXMLData(std::istream &in);
    void CreateObjectFromXMLData(const std::string &in);

    // force to skip condition check
    void LoadGroup(const std::string &name, bool force = true);

    // if push is true, and name is given, keeps it
    // returns true if node is stored, false otherwise
    bool LoadGroup(pugi::xml_node node, bool force = false, bool push = true);

    void AddHook(const std::string &name, hookfunc_t f);
    void ExecuteHooks(const std::string &name);

    // starts a new table of Local Strings
    void BeginLocals();
    void EndLocals();
    void SetLocal(const std::string &name, const std::string &value);
    std::string GetLocal(const std::string &name) const;
    void RemoveLocal(const std::string &name);

    // expand string using the locals table
    std::string ExpandString(const std::string &str);

    void RegisterType(const std::string &name, factoryfunc_t f);

    template <typename T>
    T Get(const std::string &name) const;

    template <typename T>
    void Set(const std::string &name, const T &value);

    void Remove(const std::string &name);

    template <typename T>
    void AddNotification(const std::string &name, notiffunc_t<T> f);

    template <typename T>
    std::shared_ptr<T> GetObj(const std::string &name);

    template <typename T>
    void AddObj(const std::string &name, std::shared_ptr<T> obj);

    void FreeObj(const std::string &name);

    void CreateAlias(const std::string &name, const std::string &target);
    void DeleteAlias(const std::string &name);
    std::string GetAlias(const std::string &name) const;

    virtual ~TContext();

protected:

    struct obj_wrapper_
    {
        boost::any obj;
        std::list<std::function<void ()>> notif;
    };

    std::map<std::string, factoryfunc_t> factories_;
    std::map<std::string, obj_wrapper_> objects_;
    std::map<std::string, std::list<hookfunc_t>> hooks_;
    std::list<std::shared_ptr<pugi::xml_document>> docs_;
    std::list<pugi::xml_node> groups_;
    std::list<std::map<std::string, std::string>> locals_;
    std::map<std::string, std::string> aliases_;
    sel::State luastate_;

};

#include "Context.ipp" // template functions

/**
 * Following macros and funcs are considered to be used in type handlers
 * For macros to work, ctx as TContext and node as pugi::xml_node must exist
 */

inline static bool stob(const std::string &s)
{
    if (s.empty()) return false;
    std::string test = "1tTyY+";
    if (test.find(s[0]) == std::string::npos /* not found */) return false;
    return true;
}

#define CTX_NODE_ATTR_STR(name, default)     ctx.ExpandString(node.attribute(#name).as_string(default))
#define CTX_NODE_ATTR_DOUBLE(name, default)  std::stod(ctx.ExpandString(node.attribute(#name).as_string( #default )))
#define CTX_NODE_ATTR_INT(name, default)     std::stoi(ctx.ExpandString(node.attribute(#name).as_string( #default )))
#define CTX_NODE_ATTR_LONG(name, default)    std::stol(ctx.ExpandString(node.attribute(#name).as_string( #default )))
#define CTX_NODE_ATTR_BOOL(name, default)    stob(ctx.ExpandString(node.attribute(#name).as_string( #default )))
#define CTX_NODE_TEXT()                      ctx.ExpandString(node.text().get())

}

#endif // _CONTEXT_HPP_INCLUDED
