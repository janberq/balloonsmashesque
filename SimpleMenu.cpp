#include "SimpleMenu.hpp"

namespace BalloonSmashEsque
{

TSimpleMenu::TSimpleMenu(
    std::shared_ptr<SDL2pp::Renderer> renderer,
    std::shared_ptr<SDL2pp::Font> font,
    std::shared_ptr<SDL2pp::Texture> buttonnormaltexture,
    std::shared_ptr<SDL2pp::Texture> buttonhovertexture,
    std::shared_ptr<SDL2pp::Texture> buttonpressedtexture,
    const SDL_Color &foreground,
    const SDL2pp::Point &menupos,
    int buttonseparation,
    int imageleftrightmargin)
{
    render_ = renderer;
    font_ = font;
    buttonnormal_ = buttonnormaltexture;
    buttonhover_ = buttonhovertexture;
    buttonpressed_ = buttonpressedtexture;
    fg_ = foreground;
    pos_ = menupos;
    buttonseparation_ = buttonseparation;
    imageleftrightmargin_ = imageleftrightmargin;

    buttonwidth_ = buttonnormal_->GetWidth();
    buttonheight_ = buttonnormal_->GetHeight();

    // TODO add some exception handling
}

TSimpleMenu & TSimpleMenu::operator()(
    std::size_t id,
    const std::string& text,
    std::shared_ptr<SDL2pp::Texture> buttonimage,
    TSimpleMenu::EImageAlign imagealign)
{
    button_t button;

    int wi, he;
    wi = buttonnormal_->GetWidth();
    he = buttonnormal_->GetHeight();

    auto draw_button_image = [=]() {
        if (!buttonimage) return ;
        int imwi, imhe;
        imwi = buttonimage->GetWidth();
        imhe = buttonimage->GetHeight();
        SDL2pp::Rect dst;
        dst.w = imwi;
        dst.h = imhe;
        dst.y = (he-imhe) / 2;
        switch (imagealign)
        {
            case IMAGE_ALIGN_CENTER:
                dst.x = (wi-imwi) / 2;
                break;
            case IMAGE_ALIGN_LEFT:
                dst.x = imageleftrightmargin_;
                break;
            case IMAGE_ALIGN_RIGHT:
                dst.x = imwi - imageleftrightmargin_  -1;
                break;
        }
        render_->Copy(*buttonimage, SDL2pp::NullOpt, dst);
    };

    auto draw_button_text = [=]() {
        SDL2pp::Texture text_texture { *render_, font_->RenderUTF8_Blended(text, fg_) };
        int textwi, texthe;
        textwi = text_texture.GetWidth();
        texthe = text_texture.GetHeight();
        render_->Copy(text_texture, SDL2pp::NullOpt,
            SDL2pp::Rect((wi-textwi)/2, (he-texthe)/2,
            textwi, texthe));
    };

    // normal state of the button
    {
        button.normal = std::make_shared<SDL2pp::Texture>(
            *render_, SDL_PIXELFORMAT_RGBA8888,
            SDL_TEXTUREACCESS_TARGET,
            buttonnormal_->GetWidth(),
            buttonnormal_->GetHeight());
        button.normal->SetBlendMode(SDL_BLENDMODE_BLEND);
        render_->SetTarget(*button.normal);
        render_->SetDrawBlendMode(SDL_BLENDMODE_BLEND);
        render_->Clear();
        render_->Copy(*buttonnormal_, SDL2pp::NullOpt, SDL2pp::NullOpt);
        draw_button_image();
        draw_button_text();
    }

    // hover state of the button
    {
        button.hover = std::make_shared<SDL2pp::Texture>(
            *render_, SDL_PIXELFORMAT_RGBA8888,
            SDL_TEXTUREACCESS_TARGET,
            buttonnormal_->GetWidth(),
            buttonnormal_->GetHeight());
        button.hover->SetBlendMode(SDL_BLENDMODE_BLEND);
        render_->SetTarget(*button.hover);
        render_->SetDrawBlendMode(SDL_BLENDMODE_BLEND);
        render_->Clear();
        render_->Copy(*buttonhover_, SDL2pp::NullOpt, SDL2pp::NullOpt);
        draw_button_image();
        draw_button_text();
    }

    // hover state of the button
    {
        button.pressed = std::make_shared<SDL2pp::Texture>(
            *render_, SDL_PIXELFORMAT_RGBA8888,
            SDL_TEXTUREACCESS_TARGET,
            buttonnormal_->GetWidth(),
            buttonnormal_->GetHeight());
        button.pressed->SetBlendMode(SDL_BLENDMODE_BLEND);
        render_->SetTarget(*button.pressed);
        render_->SetDrawBlendMode(SDL_BLENDMODE_BLEND); 
        render_->Clear();
        render_->Copy(*buttonpressed_, SDL2pp::NullOpt, SDL2pp::NullOpt);
        draw_button_image();
        draw_button_text();
    }

    render_->SetTarget();
    button.id = id;
    buttons_.push_back(button);

    return *this;
}

void TSimpleMenu::OnClick(std::function<void (std::size_t)> onclick)
{
    onclick_ = onclick;
}

int TSimpleMenu::GetHeight() const
{
    return (buttonnormal_->GetHeight() + buttonseparation_) * buttons_.size()
        - buttonseparation_;
}

int TSimpleMenu::GetWidth() const
{
    return buttonnormal_->GetWidth();
}


void TSimpleMenu::Handle(SDL_Event& e)
{
    auto getbutton = [this](int x, int y) {
        if (x >= pos_.x && x <= pos_.x + buttonwidth_ && y >= pos_.y)
        {
            int n = (y - pos_.y + 1) / (buttonheight_ + buttonseparation_);
            int remainder = (y - pos_.y) % (buttonheight_ +
                buttonseparation_);
            if (n < buttons_.size())
            {
                // handle the separation
                if (remainder < buttonheight_)
                {
                    return buttons_.cbegin() + n;
                }
                else
                {
                    return buttons_.cend();
                }
            }
            else
            {
                return buttons_.cend();
            }
        }
        else
        {
            return buttons_.cend();
        }
    };
    if (e.type == SDL_MOUSEMOTION)
    {
        hovered_ = getbutton(e.motion.x, e.motion.y);
    }
    else if (e.type == SDL_MOUSEBUTTONDOWN)
    {
        pressed_ = getbutton(e.button.x, e.button.y);
    }
    else if (e.type == SDL_MOUSEBUTTONUP)
    {
        if (getbutton(e.button.x, e.button.y) == pressed_)
        {
            // trigger click event
            if (onclick_) onclick_(pressed_->id);
        }
        pressed_ = buttons_.cend();
    }
}

void TSimpleMenu::Render()
{
    int btnwi = buttonnormal_->GetWidth();
    int btnhe = buttonnormal_->GetHeight();
    SDL2pp::Rect dst;
    dst.x = pos_.x;
    dst.y = pos_.y;
    dst.w = btnwi;
    dst.h = btnhe;
    for (auto it = buttons_.begin(); it != buttons_.end(); it++)
    {
        if (it == pressed_)
        {
            render_->Copy(*it->pressed, SDL2pp::NullOpt, dst);
        }
        else if (it == hovered_)
        {
            render_->Copy(*it->hover, SDL2pp::NullOpt, dst);
        }
        else
        {
            render_->Copy(*it->normal, SDL2pp::NullOpt, dst);
        }
        dst.y += btnhe + buttonseparation_;
    }
}

}
