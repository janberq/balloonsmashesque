#ifndef STATICCOMPONENTS_HPP_INCLUDED
#define STATICCOMPONENTS_HPP_INCLUDED

#include "Component.hpp"

#include <SDL2pp/Renderer.hh>
#include <SDL2pp/Texture.hh>
#include <SDL2pp/Font.hh>
#include <memory>
#include <string>

namespace BalloonSmashEsque
{

class TStaticImage : public TComponent
{

public:

    TStaticImage(
        std::shared_ptr<SDL2pp::Renderer> renderer = nullptr,
        std::shared_ptr<SDL2pp::Texture> img = nullptr,
        SDL2pp::Point pos = {0, 0}
    );

    int GetWidth() const override;
    int GetHeight() const override;

    std::shared_ptr<SDL2pp::Texture> GetImage();
    void SetImage(std::shared_ptr<SDL2pp::Texture> img);

    void Render() override;

protected:

    std::shared_ptr<SDL2pp::Renderer> renderer_;
    std::shared_ptr<SDL2pp::Texture> img_;

};

class TStaticText : public TComponent
{

public:

    TStaticText(
        std::shared_ptr<SDL2pp::Renderer> renderer = nullptr,
        std::shared_ptr<SDL2pp::Font> font = nullptr,
        SDL_Color fg = {0, 0, 0, 255},
        const std::string &text = "",
        SDL2pp::Point pos = {0, 0}
    );

    int GetWidth() const override;
    int GetHeight() const override;

    void Render() override;

    std::string GetText() const;
    void SetText(const std::string &text);

    std::shared_ptr<SDL2pp::Font> GetFont();
    void SetFont(std::shared_ptr<SDL2pp::Font> font);

    SDL_Color GetForegroundColor() const;
    void SetForegroundColor(const SDL_Color &fg);

private:

    void Redraw();

    std::shared_ptr<SDL2pp::Renderer> renderer_;
    std::shared_ptr<SDL2pp::Font> font_;
    SDL_Color fg_;
    std::shared_ptr<SDL2pp::Texture> texture_;
    std::string text_;

};

}

#endif // STATICCOMPONENTS_HPP_INCLUDED
