#include "Boxer.hpp"
#include "Context.hpp"
#include <sstream>
#include <regex>

namespace BalloonSmashEsque
{

void LoadBoxerToContext(TContext &ctx, pugi::xml_node node)
{
    /* recursively traverse */
    std::function<TBoxerNode * (pugi::xml_node)> loadNode =
    [&ctx, &loadNode](pugi::xml_node node) -> TBoxerNode *
    {
        TBoxerNode *result = nullptr;
        if (std::string(node.name()) == "boxer")
        {
            TBoxer *res = new TBoxer;
            result = res;
            res->w = CTX_NODE_ATTR_DOUBLE(w, 0);
            res->h = CTX_NODE_ATTR_DOUBLE(h, 0);
            res->prop = CTX_NODE_ATTR_DOUBLE(prop, -1);
            res->expand = CTX_NODE_ATTR_BOOL(expand, true);
            res->id = CTX_NODE_ATTR_STR(id, "");
            res->usex = CTX_NODE_ATTR_BOOL(usex, false);
            res->xw = CTX_NODE_ATTR_DOUBLE(xw, -1);
            res->xh = CTX_NODE_ATTR_DOUBLE(xh, -1);
            std::string xoffset_str = CTX_NODE_ATTR_STR(xoffset, "(0, 0)");
            std::string typ = CTX_NODE_ATTR_STR(type, "TYPE_VERT");
            if (typ == "TYPE_VERT") res->type = TBoxer::TYPE_VERT;
            else if (typ == "TYPE_HORZ") res->type = TBoxer::TYPE_HORZ;
            else throw std::runtime_error("load boxer: invalid boxer type");

            /* alignment */
            res->xalign = 0; // yes, always initialize variables first!
            std::stringstream ssalign(CTX_NODE_ATTR_STR(xalign, ""));
            std::string algn;
            while (std::getline(ssalign, algn, '|'))
            {
#define CHECK_FLAG(F) if (algn == #F) res->xalign |= TBoxer::F;
                        CHECK_FLAG(VERT_TOP)
                else    CHECK_FLAG(VERT_BOTTOM)
                else    CHECK_FLAG(VERT_CENTER)
                else    CHECK_FLAG(HORZ_LEFT)
                else    CHECK_FLAG(HORZ_RIGHT)
                else    CHECK_FLAG(HORZ_CENTER)
                else throw std::runtime_error("load boxer: invalid align flag");
#undef CHECK_FLAG
            }
            /* xoffset */
            std::regex rgx { "\\(\\s*((?:-|\\+)?\\d+(?:\\.\\d+)?)\\s*,\\s*((?:-|\\+)?\\d+(?:\\.\\d+)?)\\s*\\)" };
            std::smatch sm;
            if (std::regex_match(xoffset_str, sm, rgx))
            {
                res->xoffsetx = std::stod(sm[1]);
                res->xoffsety = std::stod(sm[2]);
            }
            else
            {
                throw std::runtime_error("load boxer: invalid offset format");
            }

            if (std::string(node.first_child().name()) == "alt")
            {
                for (auto &alt : node.children()) // now, every node is considered to be alt
                {
                    res->alts.emplace_back();
                    auto subs = res->alts.back();
                    for (auto &sub : node.children())
                    {
                        subs.push_back(loadNode(sub));
                    }
                }
            }
            else
            {
                res->alts.emplace_back();
                auto &subs = res->alts.back();
                for (auto &sub : node.children())
                {
                    subs.push_back(loadNode(sub));
                }
            }

            res->fit = CTX_NODE_ATTR_BOOL(fit, false);
        }
        else if (std::string(node.name()) == "comp")
        {
            TBoxerComponent *res = new TBoxerComponent;
            result = res;
            /* standard things */
            res->w = CTX_NODE_ATTR_DOUBLE(w, 0);
            res->h = CTX_NODE_ATTR_DOUBLE(h, 0);
            res->prop = CTX_NODE_ATTR_DOUBLE(prop, -1);
            res->expand = CTX_NODE_ATTR_BOOL(expand, true);
            res->id = CTX_NODE_ATTR_STR(id, "");
            std::string offset_str = CTX_NODE_ATTR_STR(offset, "(0, 0)");
            /* alignment */
            res->align = 0; // yes, always initialize variables first!
            std::stringstream ssalign(CTX_NODE_ATTR_STR(align, ""));
            std::string algn;
            while (std::getline(ssalign, algn, '|'))
            {
#define CHECK_FLAG(F) if (algn == #F) res->align |= TBoxerComponent::F;
                        CHECK_FLAG(VERT_TOP)
                else    CHECK_FLAG(VERT_BOTTOM)
                else    CHECK_FLAG(VERT_CENTER)
                else    CHECK_FLAG(HORZ_LEFT)
                else    CHECK_FLAG(HORZ_RIGHT)
                else    CHECK_FLAG(HORZ_CENTER)
                else throw std::runtime_error("load boxer: invalid align flag");
#undef CHECK_FLAG
            }
            /* offset */
            std::regex rgx { "\\(\\s*((?:-|\\+)?\\d+(?:\\.\\d+)?)\\s*,\\s*((?:-|\\+)?\\d+(?:\\.\\d+)?)\\s*\\)" };
            std::smatch sm;
            if (std::regex_match(offset_str, sm, rgx))
            {
                res->offsetx = std::stod(sm[1]);
                res->offsety = std::stod(sm[2]);
            }
            else
            {
                throw std::runtime_error("load boxer: invalid offset format");
            }

            res->fit = CTX_NODE_ATTR_BOOL(fit, false);
        }
        else if (std::string(node.name()) == "space")
        {
            TBoxerSpacer *res = new TBoxerSpacer;
            result = res;
            /* standard things */
            res->w = CTX_NODE_ATTR_DOUBLE(w, 0);
            res->h = CTX_NODE_ATTR_DOUBLE(h, 0);
            res->prop = CTX_NODE_ATTR_DOUBLE(prop, -1);
            res->expand = CTX_NODE_ATTR_BOOL(expand, true);
            res->id = CTX_NODE_ATTR_STR(id, "");
        }
        else
        {
            throw std::runtime_error("load boxer: invalid tag: " + std::string(node.name()));
        }
        return result;
    };
    std::string objName = CTX_NODE_ATTR_STR(name, "");
    TBoxer *boxer = dynamic_cast<TBoxer *>(loadNode(node));
    std::shared_ptr<TBoxer> obj;
    obj.reset(boxer);
    ctx.AddObj<TBoxer>(objName, obj);
}

void RegisterBoxerToContext(TContext &ctx)
{
    ctx.RegisterType("boxer", LoadBoxerToContext);
}

}
