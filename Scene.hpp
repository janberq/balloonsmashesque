#ifndef _SCANE_HPP_INCLUDED
#define _SCANE_HPP_INCLUDED

#include <SDL2/SDL.h>

namespace BalloonSmashEsque
{

class TGame;

class TScene
{

public:

    virtual void Render();
    virtual void Handle(SDL_Event &e);

    virtual void OnPause();
    virtual void OnResume();

    virtual void OnCurrent();
    virtual void OnNotCurrent();

    virtual ~TScene();

};

}

#endif // _SCANE_HPP_INCLUDED
