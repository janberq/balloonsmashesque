/**
 * A very simple box based layout system for generic purposes
 */

#ifndef _BOXER_HPP_INCLUDED
#define _BOXER_HPP_INCLUDED

#include <list>
#include <string>
#include <stdexcept>

#include <ostream>

namespace BalloonSmashEsque
{

class TComponent;

class TBoxerNode
{

public:

    double w { 0.0 }, h { 0.0 }, x { 0.0 }, y { 0.0 };
    double prop { -1.0 };
    bool expand { false };

    std::string id;

    virtual void Prepare();
    virtual void DoBoxing();

    virtual void Dump(std::ostream &os, unsigned int leadingspace = 0);

    virtual ~TBoxerNode();

};

class TBoxerComponent : public TBoxerNode
{

public:

    typedef short alignflags_t;

    /* alignment flags */
    static constexpr alignflags_t VERT_TOP    = 0b000001;
    static constexpr alignflags_t VERT_BOTTOM = 0b000010;
    static constexpr alignflags_t VERT_CENTER = 0b000100;
    static constexpr alignflags_t HORZ_LEFT   = 0b001000;
    static constexpr alignflags_t HORZ_RIGHT  = 0b010000;
    static constexpr alignflags_t HORZ_CENTER = 0b100000;

    TComponent *component { nullptr };
    double offsetx{ 0.0 }, offsety { 0.0 };
    alignflags_t align { VERT_TOP | HORZ_LEFT };
    bool fit { false };

    void Prepare() override;
    void DoBoxing() override;

    void Dump(std::ostream &os, unsigned int leadingspace = 0) override;

};

class TBoxerSpacer : public TBoxerNode
{

public:

    void DoBoxing() override;

    void Dump(std::ostream &os, unsigned int leadingspace = 0) override;

};

class TBoxer : public TBoxerNode
{

public:

    enum
    {
        TYPE_VERT,
        TYPE_HORZ
    } type;

    typedef short alignflags_t;

    /* alignment flags */
    static constexpr alignflags_t VERT_TOP    = 0b000001;
    static constexpr alignflags_t VERT_BOTTOM = 0b000010;
    static constexpr alignflags_t VERT_CENTER = 0b000100;
    static constexpr alignflags_t HORZ_LEFT   = 0b001000;
    static constexpr alignflags_t HORZ_RIGHT  = 0b010000;
    static constexpr alignflags_t HORZ_CENTER = 0b100000;

    /* extended features of the sizer */
    bool usex { false };
    double xw { 0.0 }, xh { 0.0 };
    double xoffsetx{ 0.0 }, xoffsety { 0.0 };
    alignflags_t xalign { VERT_TOP | HORZ_LEFT };
    bool fit {false};

    /* alternatives, each alternative contains a sub-sizers */
    std::list<std::list<TBoxerNode *>> alts;

    void Prepare() override;
    void DoBoxing() override;

    void Dump(std::ostream &os, unsigned int leadingspace = 0) override;

    TBoxerNode *FindNodeWithID(const std::string &s);
    void BindComponent(const std::string &name, TComponent *comp);

    virtual ~TBoxer();

};

class TContext;
void RegisterBoxerToContext(TContext &ctx);

}

#endif // _BOXER_HPP_INCLUDED
