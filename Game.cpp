#include "Game.hpp"

#include <utility>
#include <SDL2/SDL2_gfxPrimitives.h>

namespace BalloonSmashEsque
{

TGame::TGame(int argc, char **argv)
{
    current_scene_ = nullptr;
    continue_ = true;
    paused_ = false;
    running_ = false;
    norun_ = false;

    exit_code_ = 0;

    Init(argc, argv);
}

TGame::~TGame()
{
    Deinit();
    // get rid off the scenes
    for (auto &o : scenes_)
    {
        delete o.second;
    }
}

std::string TGame::GetResourcePath(const std::string& path) const
{
    return datadir_ + "/" + path;
}

void TGame::AddScene(const std::string& name, TScene* scene)
{
    scenes_[name] = scene;
}

void TGame::SetCurrentScene(const std::string& scene)
{
    auto it = scenes_.find(scene);
    if (it == scenes_.end())
    {
        throw TException("Scene not found!");
    }
    if (current_scene_ != nullptr)
    {
        current_scene_->OnNotCurrent();
    }
    current_scene_ = it->second;
    current_scene_->OnCurrent();
}

void TGame::SetCurrentDialog(TDialog *dlg)
{
    current_dialog_ = dlg;
}

std::shared_ptr<SDL2pp::Renderer> TGame::GetRenderer()
{
    return renderer_;
}

std::shared_ptr<SDL2pp::Window> TGame::GetWindow()
{
    return window_;
}

int TGame::Run()
{
    if (norun_)
        return exit_code_;
    class __running_lock
    {
        bool &b_;
    public:
        __running_lock(bool &b) : b_ {b}
        {
            b_ = true;
        }
        ~__running_lock()
        {
            b_ = false;
        }
    } running_lock(running_); // in case of an exception
    continue_ = true;
    if (!current_scene_) throw TException("No current scene is set!");
    if (!window_) throw TException("Invalid window!");
    if (!renderer_) throw TException("Invalid renderer!");
    SDL_Event e;
    while (continue_)
    {
        if (!current_dialog_)
        {
            while (SDL_PollEvent(&e) > 0)
            {
                current_scene_->Handle(e);
            }
        }
        else
        {
            while (SDL_PollEvent(&e) > 0)
            {
                current_dialog_->Handle(e);
            }
        }
        renderer_->Clear();
        current_scene_->Render();
        if (current_dialog_)
        {
            renderer_->SetDrawBlendMode(SDL_BLENDMODE_BLEND);
            auto shade = current_dialog_->GetShadeColor();
            renderer_->SetDrawColor(shade.r, shade.g, shade.b, shade.a);
            renderer_->FillRect(SDL2pp::Rect(
                SDL2pp::Point(0, 0), window_->GetSize()));
            current_dialog_->Render();
        }
        renderer_->Present();
    }
    return exit_code_;
}

void TGame::Pause()
{
    if (paused_)
    {
        throw TException("Already paused!");
    }
    else
    {
        paused_ = true;
        current_scene_->OnPause();
        Set<bool>("game.paused", true);
    }
}

void TGame::Resume()
{
    if (paused_)
    {
        paused_ = false;
        current_scene_->OnResume();
        Set<bool>("game.paused", false);
    }
    else
    {
        throw TException("Not paused!");
    }
}

void TGame::Stop()
{
    continue_ = false;
}

bool TGame::IsRunning() const
{
    return running_;
}

bool TGame::IsPaused() const
{
    return paused_;
}

}
