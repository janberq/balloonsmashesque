#include "RegisterSDL2pp.hpp"
#include "Context.hpp"

#include <SDL2pp/Texture.hh>
#include <SDL2pp/Renderer.hh>
#include <SDL2pp/Font.hh>
#include <SDL2/SDL.h>

#include <cstring> // for ::tolower
#include <regex>
#include <stdexcept>
#include <memory>
#include <sstream>
#include <vector>

namespace BalloonSmashEsque
{

static void LoadSDL2ppTextureToContext(
    std::shared_ptr<SDL2pp::Renderer> renderer,
    const std::string &datadir,
    TContext &ctx,
    pugi::xml_node node)
{
    /* if this is not a game context, and renderer is not loaded
     * then an exception will be thrown resulting a crash
     */
    std::string objName = CTX_NODE_ATTR_STR(name, "");
    std::string path = CTX_NODE_ATTR_STR(path, "");
    std::string srcrect_str = CTX_NODE_ATTR_STR(srcrect, "full");
    std::string size_str = CTX_NODE_ATTR_STR(size, "same");

    if (CTX_NODE_ATTR_BOOL(relative, true)) path = datadir + "/" + path;

    if (srcrect_str == "full" && size_str == "same")
    {
        ctx.AddObj(objName, std::make_shared<SDL2pp::Texture>(*renderer, path));
        return ;
    }

    SDL2pp::Texture src { *renderer, path };

    SDL2pp::Rect srcrect;
    SDL2pp::Rect destrect;
    destrect.x = 0; destrect.y = 0;

    if (srcrect_str == "full")
    {
        srcrect.x = 0;
        srcrect.y = 0;
        srcrect.w = src.GetWidth();
        srcrect.h = src.GetHeight();
    }
    else
    {
        std::regex rgx_srcrect { "(\\d+(?:\\.\\d+)?)x(\\d+(?:\\.\\d+)?)\\+(\\d+(?:\\.\\d+)?)\\+(\\d+(?:\\.\\d+)?)" };
        std::smatch sm;
        if (std::regex_match(srcrect_str, sm, rgx_srcrect))
        {
            srcrect = SDL2pp::Rect {
                (int) std::stod(sm[3]) /* x */, 
                (int) std::stod(sm[4]) /* y */,
                (int) std::stod(sm[1]) /* w */,
                (int) std::stod(sm[2]) /* h */};
        }
        else
        {
            throw std::runtime_error("load sdl2pp texture error: not valid srcrect format");
        }
    }

    std::regex rgx_sizescale { "scale\\s+(\\d+(?:\\.\\d+)?)\\s+(\\d+(?:\\.\\d+)?)" };
    std::regex rgx_size      { "(\\d+(?:\\.\\d+)?)x(\\d+(?:\\.\\d+)?)" };
    std::smatch sm;

    if (size_str == "same")
    {
        destrect.w = srcrect.w;
        destrect.h = srcrect.h;
    }
    else if (std::regex_match(size_str, sm, rgx_sizescale))
    {
        destrect.w = srcrect.w * std::stod(sm[1]);
        destrect.h = srcrect.h * std::stod(sm[2]);
    }
    else if (std::regex_match(size_str, sm, rgx_size))
    {
        destrect.w = std::stod(sm[1]);
        destrect.h = std::stod(sm[2]);
    }
    else
    {
        throw std::runtime_error("load sdl2pp texture error: not valid size format");
    }
    auto texture = std::make_shared<SDL2pp::Texture>(
        *renderer,
        SDL_PIXELFORMAT_RGBA8888,
        SDL_TEXTUREACCESS_TARGET,
        destrect.w,
        destrect.h);
    texture->SetBlendMode(SDL_BLENDMODE_BLEND);
    renderer->SetTarget(*texture);
    renderer->SetDrawBlendMode(SDL_BLENDMODE_BLEND);
    renderer->Copy(src, srcrect, destrect);
    renderer->SetTarget(); // restore target
    ctx.AddObj(objName, texture);
}

void RegisterSDL2ppTextureToContext(TContext &ctx)
{
    auto renderer = ctx.GetObj<SDL2pp::Renderer>("game.renderer");
    auto datadir = ctx.Get<std::string>("config.str.datadir");

    ctx.RegisterType(
        "texture",
        [renderer, datadir](TContext &ctx, pugi::xml_node node)
            /* using cached values may speed up the app */
        {
            LoadSDL2ppTextureToContext(renderer, datadir, ctx, node);
        });
}

static void LoadSDL2ppFontToContext(
    const std::string &datadir,
    TContext &ctx,
    pugi::xml_node node)
{
    std::string objName = CTX_NODE_ATTR_STR(name, "");
    std::string path = CTX_NODE_ATTR_STR(path, "");
    int ptsize = CTX_NODE_ATTR_INT(ptsize, 16);
    long index = CTX_NODE_ATTR_LONG(index, 0);

    if (CTX_NODE_ATTR_BOOL(relative, true)) path = datadir + "/" + path;

    auto font = std::make_shared<SDL2pp::Font>(path, ptsize, index);
    ctx.AddObj(objName, font);
}

void RegisterSDL2ppFontToContext(TContext& ctx)
{

    auto datadir = ctx.Get<std::string>("config.str.datadir");

    ctx.RegisterType(
        "font",
        [datadir](TContext &ctx, pugi::xml_node node)
            /* using cached values may speed up the app */
        {
            LoadSDL2ppFontToContext(datadir, ctx, node);
        });
}

static void LoadSDLColorToContext(
    TContext &ctx,
    const std::map<std::string, SDL_Color> &colors,
    pugi::xml_node node)
{
    std::string objName = CTX_NODE_ATTR_STR(name, "");
    std::string color = CTX_NODE_ATTR_STR(color, "RGB (  0 , 0 , 0 ) ");
    SDL_Color result;

    /* extract components */

    std::size_t repeating_count = std::count(color.begin(), color.end(), ',');
    std::ostringstream oss;
    oss << R"RGX(\s*(\w+)\s*\()RGX";
    for (int i = 0; i < repeating_count; ++i)
    {
        oss << R"RGX((?:\s*(\d+(?:\.\d+)?)\s*,))RGX";
    }
    oss << R"RGX(\s*(\d+(?:\.\d+)?)\s*\)\s*)RGX";
    auto regex_string = oss.str();
    std::vector<double> components;
    components.reserve(repeating_count + 1);
    std::regex rgx { regex_string };
    std::smatch sm;
    if (std::regex_match(color, sm, rgx))
    {
        std::string color_space = sm[1];
        for (int i = 2; i < sm.size(); ++i)
        {
            components.push_back(std::stod(sm[i]));
        }

        if (color_space == "rgb" || color_space == "RGB")
        {
            result.a = 255;
            result.r = components[0];
            result.g = components[1];
            result.b = components[2];
        }
        else if (color_space == "rgba" || color_space == "RGBA")
        {
            result.a = components[3];
            result.r = components[0];
            result.g = components[1];
            result.b = components[2];
        }
        else
        {
            // TODO: ADD SUPPORT FOR OTHER COLOR SPACES
            throw std::runtime_error("load SDL_Color: unknown color space");
        }
    }
    else
    {
        std::transform(color.begin(), color.end(), color.begin(), ::tolower);
        auto it = colors.find(color);
        if (it != colors.end())
        {
            result = it->second;
        }
        else
        {
            throw std::runtime_error("load SDL_Color: invalid colour format");
        }
    }
    ctx.AddObj(objName, std::make_shared<SDL_Color>(result));
}

void RegisterSDLColorToContext(TContext &ctx)
{
    std::map<std::string, SDL_Color> colors;
#define ADD_COLOR(n, x, y, z) colors[ n ] = SDL_Color { x , y , z , 255 };
    ADD_COLOR("black", 0, 0, 0)
    ADD_COLOR("white", 255, 255, 255)
    ADD_COLOR("red", 255, 0, 0)
    ADD_COLOR("lime", 0, 255, 0)
    // ADD_COLOR("green", 0, 255, 0)
    ADD_COLOR("blue", 0, 0, 255)
    ADD_COLOR("yellow", 255, 255, 0)
    ADD_COLOR("cyan", 0, 255, 255)
    ADD_COLOR("aqua", 0, 255, 255)
    ADD_COLOR("magenta", 255, 0, 255)
    ADD_COLOR("fuchsia", 255, 0, 255)
    ADD_COLOR("silver", 192, 192, 192)
    ADD_COLOR("gray", 128, 128, 128)
    ADD_COLOR("maroon", 128, 0, 0)
    ADD_COLOR("olive", 128, 128, 0)
    ADD_COLOR("green", 0, 128, 0)
    ADD_COLOR("purple", 128, 0, 128)
    ADD_COLOR("teal", 0, 128, 128)
    ADD_COLOR("navy", 0, 0, 128)
#undef ADD_COLOR
    ctx.RegisterType("color",
        [colors = std::move(colors)](TContext &ctx, pugi::xml_node node)
        {
            LoadSDLColorToContext(ctx, colors, node);
        });
}

}
