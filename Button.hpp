#ifndef _BUTTON_HPP_INCLUDED
#define _BUTTON_HPP_INCLUDED

#include <memory>
#include <functional>
#include <SDL2pp/SDL2pp.hh>

#include "Component.hpp"

namespace BalloonSmashEsque
{

class TButton : public TComponent
{

public:

    TButton(
        std::shared_ptr<SDL2pp::Renderer> renderer,
        std::shared_ptr<SDL2pp::Texture> normal,
        const SDL2pp::Point &pos = { 0, 0 },
        std::shared_ptr<SDL2pp::Texture> hover = nullptr,
        std::shared_ptr<SDL2pp::Texture> pressed = nullptr,
        std::shared_ptr<SDL2pp::Texture> image = nullptr);

    void OnClick(std::function<void ()> onclick);

    void Render() override;
    void Handle(SDL_Event &e) override;

    int GetWidth() const override;
    int GetHeight() const override;

    ~TButton();

private:

    std::shared_ptr<SDL2pp::Renderer> renderer_;

    std::shared_ptr<SDL2pp::Texture> normal_;
    std::shared_ptr<SDL2pp::Texture> hover_;
    std::shared_ptr<SDL2pp::Texture> pressed_;

    std::function<void ()> onclick_;

    int wi_, he_;

    enum
    {
        STATE_NORMAL,
        STATE_HOVER,
        STATE_PRESSED
    } state_;

};

class TButtonFactory
{
public:

    TButtonFactory(
        std::shared_ptr<SDL2pp::Renderer> renderer,
        std::shared_ptr<SDL2pp::Texture> normal,
        std::shared_ptr<SDL2pp::Texture> hover = nullptr,
        std::shared_ptr<SDL2pp::Texture> pressed = nullptr);

    std::shared_ptr<TButton> operator()(
        const SDL2pp::Point &pos,
        std::shared_ptr<SDL2pp::Texture> img = nullptr);

    int GetButtonWidth() const;
    int GetButtonHeight() const;

private:

    std::shared_ptr<SDL2pp::Renderer> renderer_;
    std::shared_ptr<SDL2pp::Texture> normal_;
    std::shared_ptr<SDL2pp::Texture> hover_;
    std::shared_ptr<SDL2pp::Texture> pressed_;

};

}

#endif
