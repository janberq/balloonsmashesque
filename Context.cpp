#include "Context.hpp"

#include <regex>
#include <sstream>

namespace BalloonSmashEsque
{

TContext::TContext() : luastate_ { true }
{
    // register handlers for command types

    auto type_str = [](TContext &ctx, pugi::xml_node node)
    {
        std::string objName = CTX_NODE_ATTR_STR(name, "");
        if (node.attribute("value"))
        {
            ctx.Set<std::string>(objName, CTX_NODE_ATTR_STR(value, ""));
        }
        else
        {
            ctx.Set<std::string>(objName, CTX_NODE_TEXT());
        }
    };

    auto type_string = [](TContext &ctx, pugi::xml_node node)
    {
        std::string objName = CTX_NODE_ATTR_STR(name, "");
        if (node.attribute("value"))
        {
            ctx.AddObj(objName, std::make_shared<std::string>(CTX_NODE_ATTR_STR(value, "")));
        }
        else
        {
            ctx.AddObj(objName, std::make_shared<std::string>(CTX_NODE_TEXT()));
        }
    };

    auto type_int = [](TContext &ctx, pugi::xml_node node)
    {
        std::string objName = CTX_NODE_ATTR_STR(name, "");
        ctx.Set<int>(objName, CTX_NODE_ATTR_INT(value, 0));
    };

    auto type_integer = [](TContext &ctx, pugi::xml_node node)
    {
        std::string objName = CTX_NODE_ATTR_STR(name, "");
        ctx.AddObj(objName, std::make_shared<int>(CTX_NODE_ATTR_INT(value, 0)));
    };

    auto type_size_t = [](TContext &ctx, pugi::xml_node node)
    {
        std::string objName = CTX_NODE_ATTR_STR(name, "");
        std::stringstream ss(CTX_NODE_ATTR_STR(value, "0"));
        std::size_t o;
        ss >> o;
        ctx.Set<std::size_t>(objName, o);
    };

    auto type_size_t_obj = [](TContext &ctx, pugi::xml_node node)
    {
        std::string objName = CTX_NODE_ATTR_STR(name, "");
        std::stringstream ss(CTX_NODE_ATTR_STR(value, "0"));
        std::size_t o;
        ss >> o;
        ctx.AddObj(objName, std::make_shared<std::size_t>(o));
    };

    auto type_double = [](TContext &ctx, pugi::xml_node node)
    {
        std::string objName = CTX_NODE_ATTR_STR(name, "");
        ctx.Set<double>(objName, CTX_NODE_ATTR_DOUBLE(value, 0));
    };

    auto type_real = [](TContext &ctx, pugi::xml_node node)
    {
        std::string objName = CTX_NODE_ATTR_STR(name, "");
        ctx.AddObj(objName, std::make_shared<double>(CTX_NODE_ATTR_DOUBLE(value, 0)));
    };

    auto type_bool = [](TContext &ctx, pugi::xml_node node)
    {
        std::string objName = CTX_NODE_ATTR_STR(name, "");
        ctx.Set<bool>(objName, CTX_NODE_ATTR_BOOL(value, false));
    };

    auto type_boolean = [](TContext &ctx, pugi::xml_node node)
    {
        std::string objName = CTX_NODE_ATTR_STR(name, "");
        ctx.AddObj(objName, std::make_shared<bool>(CTX_NODE_ATTR_BOOL(value, false)));
    };


    // this is a pseudo-type, alias'ing functionality
    auto type_alias = [](TContext &ctx, pugi::xml_node node)
    {
        std::string aliasName = CTX_NODE_ATTR_STR(name, "");
        std::string aliasTarget = CTX_NODE_ATTR_STR(target, "");
        ctx.CreateAlias(aliasName, aliasTarget);
    };

#define _REG_TYPE(x) RegisterType( #x , type_ ## x );
    _REG_TYPE(str        )
    _REG_TYPE(string     )
    _REG_TYPE(int        )
    _REG_TYPE(integer    )
    _REG_TYPE(size_t     )
    _REG_TYPE(size_t_obj )
    _REG_TYPE(double     )
    _REG_TYPE(real       )
    _REG_TYPE(bool       )
    _REG_TYPE(boolean    )
    _REG_TYPE(alias      )
#undef _REG_TYPE

    // register lua functions

    auto luafunc_iteratenames = [this](
        std::string regex,
        sel::function<void (std::string)> f) -> void
    {
        std::regex rgx{ regex };
        for (auto &o : objects_)
        {
            std::smatch sm;
            if (std::regex_match(o.first, sm, rgx))
            {
                f(o.first); // call that function
            }
            else
            {
                // no match
            }
        }
    };

    auto luafunc_message = [this](std::string msg)
    {
        std::cout << msg << std::endl;
    };

    auto luafunc_getstr = [this](std::string name)
    {
        return Get<std::string>(name);
    };

    auto luafunc_getint = [this](std::string name)
    {
        return Get<int>(name);
    };

    auto luafunc_getdouble = [this](std::string name)
    {
        return Get<double>(name);
    };

    auto luafunc_getbool = [this](std::string name)
    {
        return Get<bool>(name);
    };

    auto luafunc_setstr = [this](std::string name, std::string val)
    {
        Set<std::string>(name, val);
    };

    auto luafunc_setint = [this](std::string name, int val)
    {
        Set<int>(name, val);
    };

    auto luafunc_setdouble = [this](std::string name, double val)
    {
        Set<double>(name, val);
    };

    auto luafunc_setbool = [this](std::string name, bool val)
    {
        Set<bool>(name, val);
    };

    auto luafunc_rem = [this](std::string name)
    {
        Remove(name);
    };

    auto luafunc_addobj = [this](std::string xml)
    {
        CreateObjectFromXMLData(xml);
    };

    auto luafunc_freeobj = [this](std::string name)
    {
        FreeObj(name);
    };

    auto luafunc_throw = [this](std::string msg)
    {
        throw std::runtime_error(msg);
    };

    // some new functions
    auto luafunc_loadgroup = [this](std::string name)
    {
        LoadGroup(name, true);
    };

    auto luafunc_loadgroupsoft = [this](std::string name)
    {
        LoadGroup(name, false);
    };

    auto luafunc_addhook = [this](std::string name, sel::function<void ()> f)
    {
        AddHook(name, [f]() mutable /*  needed for capturing by value,
                                        operator () is not const */
                            -> void { f(); });
    };

    auto luafunc_executehooks = [this](std::string name)
    {
        ExecuteHooks(name);
    };

    auto luafunc_beginlocals = [this]()
    {
        BeginLocals();
    };

    auto luafunc_endlocals = [this]()
    {
        EndLocals();
    };

    auto luafunc_setlocal = [this](std::string name, std::string val)
    {
        SetLocal(name, val);
    };

    auto luafunc_getlocal = [this](std::string name) -> std::string
    {
        return GetLocal(name);
    };

    auto luafunc_remlocal = [this](std::string name)
    {
        RemoveLocal(name);
    };

    auto luafunc_expandstr = [this](std::string str)
    {
        return ExpandString(str);
    };

#define _REG_LUA_FUNC(X) luastate_[ # X ] = luafunc_ ## X ;
    _REG_LUA_FUNC(message      )
    _REG_LUA_FUNC(iteratenames )
    _REG_LUA_FUNC(getstr       )
    _REG_LUA_FUNC(setstr       )
    _REG_LUA_FUNC(getint       )
    _REG_LUA_FUNC(setint       )
    _REG_LUA_FUNC(getdouble    )
    _REG_LUA_FUNC(setdouble    )
    _REG_LUA_FUNC(getbool      )
    _REG_LUA_FUNC(setbool      )
    _REG_LUA_FUNC(rem          )
    _REG_LUA_FUNC(addobj       )
    _REG_LUA_FUNC(freeobj      )
    _REG_LUA_FUNC(throw        )
    // new ones
    _REG_LUA_FUNC(loadgroup    )
    _REG_LUA_FUNC(loadgroupsoft)
    _REG_LUA_FUNC(addhook      )
    _REG_LUA_FUNC(executehooks )
    _REG_LUA_FUNC(beginlocals  )
    _REG_LUA_FUNC(endlocals    )
    _REG_LUA_FUNC(setlocal     )
    _REG_LUA_FUNC(getlocal     )
    _REG_LUA_FUNC(remlocal     )
    _REG_LUA_FUNC(expandstr    )
#undef _REG_LUA_FUNC
}

void TContext::ExecuteXMLData(std::istream& in)
{
    auto doc = std::make_shared<pugi::xml_document>();
    pugi::xml_parse_result result = doc->load(in);
    if (result)
    {
        if (std::distance(doc->root().begin(), doc->root().end()) == 1)
        {
            auto root = doc->root().first_child();
            if (std::string(root.name()) == "resources")
            {
                bool keep_doc = false;
                for (auto &g : root.children())
                {
                    if (std::string(g.name()) == "group")
                    {
                        keep_doc |= LoadGroup(g, false, true);
                    }
                    else if (std::string(g.name()) == "execute")
                    {
                        ExecuteLua(g.text().get());
                    }
                    else
                    {
                        throw std::runtime_error("error: bad document ('<group ...' or '<execute>' expected)");
                    }
                }
                if (keep_doc)
                {
                    docs_.push_back(doc);
                }
            }
            else
            {
                throw std::runtime_error("error: bad document ('<resources>' expected)");
            }
        }
        else
        {
            throw std::runtime_error("error: only one root node is allowed!");
        }
    }
    else
    {
        throw std::runtime_error("error: cannot parse the document. description: " + std::string(result.description()));
    }
}

void TContext::CreateObjectFromXMLData(const std::string &in)
{
    pugi::xml_document doc;
    pugi::xml_parse_result res = doc.load(in.c_str());
    if (res)
    {
        if (std::distance(doc.root().begin(), doc.root().end()) == 1)
        {
            auto root = doc.root().first_child();
            auto it = factories_.find(root.name());
            if (it != factories_.end())
            {
                if (it->second)
                {
                    (it->second)(*this, root);
                }
                else
                {
                    throw std::runtime_error("error: invalid factory function");
                }
            }
            else
            {
                throw std::runtime_error("error: not found such factory");
            }
        }
        else
        {
            throw std::runtime_error("error: only one root node is allowed");
        }
    }
    else
    {
        throw std::runtime_error("error: cannot parse the input");
    }
}

void TContext::LoadGroup(const std::string& name, bool force)
{
    for (auto &o: groups_)
    {
        if (name == o.attribute("name").as_string())
        {
            LoadGroup(o, force, false /* do not reload this */);
            break; // do not search further
        }
    }
}

bool TContext::LoadGroup(pugi::xml_node g, bool force, bool push)
{
    std::string cond = g.attribute("cond").as_string("false");
    std::string name = g.attribute("cond").as_string("");
    if (force || EvaluateLuaBool(cond))
    {
        // load this group
        for (auto &n : g.children())
        {
            auto it = factories_.find(n.name());
            if (it != factories_.end())
            {
                if (it->second)
                {
                    (it->second)(*this, n);
                }
                else
                {
                    throw std::runtime_error("error: invalid factory function");
                }
            }
            else
            {
                throw std::runtime_error("error: not found such factory");
            }
        }
    }
    else
    {
        // do not load this group
    }
    if (push && !name.empty())
    {
        groups_.push_back(g);
        return true;
    }
    return false;
}

void TContext::AddHook(const std::string& name, hookfunc_t f)
{
    hooks_[name].push_back(f);
}

void TContext::ExecuteHooks(const std::string& name)
{
    for (auto &f : hooks_[name])
    {
        f();
    }
}

void TContext::BeginLocals()
{
    locals_.emplace_back();
}

void TContext::EndLocals()
{
    locals_.pop_back();
}

void TContext::SetLocal(const std::string& name, const std::string& value)
{
    if (locals_.size() > 0)
    {
        locals_.back()[name] = value;
    }
}

std::string TContext::GetLocal(const std::string& name) const
{
    if (locals_.size() > 0)
    {
        auto map = locals_.back();
        auto it = map.find(name);
        if (it != map.end())
        {
            return it->second;
        }
        else
        {
            throw std::runtime_error("error: no such local: " + name);
        }
    }
    throw std::runtime_error("error: no last locals");
}

void TContext::RemoveLocal(const std::string& name)
{
    if (locals_.size() > 0)
    {
        auto map = locals_.back();
        auto it = map.find(name);
        if (it != map.end())
        {
            map.erase(it);
        }
        else
        {
            throw std::runtime_error("error: no such local: " + name);
        }
    }
    throw std::runtime_error("error: no last locals");
}

std::string TContext::ExpandString(const std::string& str)
{
    if (locals_.size() == 0) return str;
    auto map = locals_.back();
    using namespace std;
    regex rgx { R"RAW((\\*)\$\{([a-zA-Z0-9 _\\.]*)\})RAW" };
    string res = str;
    smatch sm;
    string::size_type begin = 0;
    while (regex_search(res.cbegin() + begin, res.cend(), sm, rgx))
    {
        if (sm.length(1) % 2 == 0)
        {
            auto pos = sm.position(2) + begin; /* position give it
                                            from the beginning */
            auto len = sm.length(2);
            string captured = sm.str(2);
            auto it = map.find(captured);
            if (it != map.end())
            {
                res.replace(pos - 2, len + 3, it->second);

                // now escape leading \ 's if they exist
                std::string replace;
                auto count = sm.length(1) / 2;
                replace.reserve(count);
                for (decltype(count) i = 0; i < count; ++i) replace += '\\';
                res.replace(begin + sm.position(1), sm.length(1), replace);
            }
            else
            {
                throw std::runtime_error("error: not found such key: "
                    + captured);
            }
        }
        else // escaped
        {
            auto n = (sm.length(1) - 1) / 2;
            std::string replace;
            replace.reserve(n);
            for (decltype(n) i = 0; i < n; i++) replace += '\\';
            res.replace(sm.position(1) + begin, sm.length(1), replace);
            begin += sm.position(1) + n + sm.length(2) + 3;
        }
    }
    return res;
}

bool TContext::EvaluateLuaBool(const std::string& s)
{
    luastate_(("__eval = " + s).c_str());
    return (bool) luastate_["__eval"];
}

void TContext::ExecuteLua(const std::string& s)
{
    luastate_(s.c_str());
}

void TContext::RegisterType(const std::string& name, TContext::factoryfunc_t f)
{
    auto it = factories_.find(name);
    if (it == factories_.end())
    {
        factories_[name] = f;
    }
    else
    {
        throw std::runtime_error("error: type with same name already registered");
    }
}


void TContext::Remove(const std::string& name)
{
    auto it = objects_.find(name);
    if (it != objects_.end())
    {
        objects_.erase(it);
    }
    else
    {
        throw std::runtime_error("error: object with such name doesn't exist");
    }
}

void TContext::FreeObj(const std::string &name)
{
    auto it = objects_.find(name);
    if (it != objects_.end())
    {
        objects_.erase(it);
    }
    else
    {
        throw std::runtime_error("error: object with such name doesn't exist");
    }
}

void TContext::CreateAlias(const std::string &name, const std::string &target)
{
    aliases_[name] = target;
}

void TContext::DeleteAlias(const std::string &name)
{
    auto it = aliases_.find(name);
    if (it != aliases_.end())
    {
        aliases_.erase(it);
    }
    throw std::runtime_error("error: alias with such name not found");
}

std::string TContext::GetAlias(const std::string& name) const
{
    auto it = aliases_.find(name);
    if (it != aliases_.end())
    {
        return it->second;
    }
    throw std::runtime_error("error: alias with such name not found");
}

TContext::~TContext()
{
}

}
