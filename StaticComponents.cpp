#include "StaticComponents.hpp"

namespace BalloonSmashEsque
{

TStaticImage::TStaticImage(
    std::shared_ptr<SDL2pp::Renderer> renderer,
    std::shared_ptr<SDL2pp::Texture> img,
    SDL2pp::Point pos) :
    renderer_(renderer),
    img_(img)
{
    pos_ = pos;
}

int TStaticImage::GetWidth() const
{
    if (img_) return img_->GetWidth();
    throw std::runtime_error("TStaticImage: null image");
}

int TStaticImage::GetHeight() const
{
    if (img_) return img_->GetHeight();
    throw std::runtime_error("TStaticImage: null image");
}

std::shared_ptr<SDL2pp::Texture> TStaticImage::GetImage()
{
    return img_;
}

void TStaticImage::SetImage(std::shared_ptr<SDL2pp::Texture> img)
{
    img_ = img;
}

void TStaticImage::Render()
{
    if (img_)
    {
        renderer_->Copy(*img_, SDL2pp::NullOpt, pos_);
    }
}

TStaticText::TStaticText(
    std::shared_ptr<SDL2pp::Renderer> renderer,
    std::shared_ptr<SDL2pp::Font> font,
    SDL_Color fg,
    const std::string& text,
    SDL2pp::Point pos) :
    renderer_(renderer),
    font_(font),
    fg_(fg)
{
    pos_ = pos;
    SetText(text);
}

int TStaticText::GetWidth() const
{
    if (texture_) return texture_->GetWidth();
    return -1;
}

int TStaticText::GetHeight() const
{
    if (texture_) return texture_->GetHeight();
    return -1;
}

void TStaticText::Render()
{
    if (texture_)
    {
        renderer_->Copy(*texture_, SDL2pp::NullOpt, pos_);
    }
}

std::string TStaticText::GetText() const
{
    return text_;
}

void TStaticText::SetText(const std::string& text)
{
    text_ = text;
    Redraw();
}

std::shared_ptr<SDL2pp::Font> TStaticText::GetFont()
{
    return font_;
}

void TStaticText::SetFont(std::shared_ptr<SDL2pp::Font> font)
{
    font_ = font;
    Redraw();
}

SDL_Color TStaticText::GetForegroundColor() const
{
    return fg_;
}

void TStaticText::SetForegroundColor(const SDL_Color& fg)
{
    fg_ = fg;
    Redraw();
}

void TStaticText::Redraw()
{
    if (font_ && renderer_)
    {
        texture_ = std::make_shared<SDL2pp::Texture>(
            *renderer_,
            font_->RenderUTF8_Blended(text_, fg_));
    }
}

}
