#include "PauseDialog.hpp"

namespace BalloonSmashEsque
{

TPauseDialog::TPauseDialog(TGame *game)
{
    // load objects
    game_ = game;
    window_ = game_->GetWindow();
    renderer_ = game_->GetRenderer();
    boxer_ = game->GetObj<TBoxer>("pauseDialog.boxer.default");
    auto font = game->GetObj<SDL2pp::Font>("pauseDialog.font.default");
    auto texturePlay = game->GetObj<SDL2pp::Texture>("pauseDialog.texture.play");
    shade_color_ = *game->GetObj<SDL_Color>("pauseDialog.color.shade");
    auto colorPauseText = *game->GetObj<SDL_Color>("pauseDialog.color.pauseText");
    auto stringPause = *game->GetObj<std::string>("pauseDialog.string.pause");
    auto button_factory = *game->GetObj<TButtonFactory>("pauseDialog.buttonfactory.default");

    pause_text_ = std::make_shared<TStaticText>(
        renderer_, font, colorPauseText, stringPause);

    play_button_ = button_factory({0, 0}, texturePlay);
    play_button_->OnClick([game]() {
        // disable this dialog
        game->Resume();
    });

    boxer_->BindComponent("playButton", play_button_.get());
    boxer_->BindComponent("pauseText", pause_text_.get());

    boxer_->w = window_->GetWidth();
    boxer_->h = window_->GetHeight();
    boxer_->Prepare();
    boxer_->DoBoxing();
}

void TPauseDialog::Render()
{
    // note: at this point, the screen should be already shaded

    pause_text_->Render();
    play_button_->Render();
}

void TPauseDialog::Handle(SDL_Event& e)
{
    if (e.type == SDL_QUIT) // handle properly
    {
        game_->Stop();
    }
    play_button_->Handle(e);
}

SDL_Color TPauseDialog::GetShadeColor() const
{
    return shade_color_;
}

TPauseDialog::~TPauseDialog()
{
}

};
