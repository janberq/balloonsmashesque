#include "Boxer.hpp"
#include "Component.hpp"

#include <iostream>

namespace BalloonSmashEsque
{

void TBoxerNode::Prepare()
{
}

void TBoxerNode::DoBoxing()
{
}

void TBoxerNode::Dump(std::ostream &os, unsigned int leadingspace)
{
    std::string space;
    space.reserve(leadingspace);
    for (int i = 0; i < leadingspace; ++i) space += ' ';
    os << space << "id: " << id << std::endl;
    os << space << "w: " << w << std::endl;
    os << space << "h: " << h << std::endl;
    os << space << "x: " << x << std::endl;
    os << space << "y: " << y << std::endl;
    os << space << "prop: " << prop << std::endl;
    os << space << "expand: " << expand << std::endl;
}

TBoxerNode::~TBoxerNode()
{
}

void TBoxerComponent::Prepare()
{
    if (fit)
    {
        w = component->GetWidth();
        h = component->GetHeight();
    }
}

void TBoxerComponent::DoBoxing()
{
    if (!component) return ;

    double compx = x;
    double compy = y;
    double compw = component->GetWidth();
    double comph = component->GetHeight();

    if ((align & VERT_TOP) == VERT_TOP)
    {
        
    }
    else if ((align & VERT_BOTTOM) == VERT_BOTTOM)
    {
        compy += h-comph;
    }
    else if ((align & VERT_CENTER) == VERT_CENTER)
    {
        compy += (h-comph) / 2; /* center */
    }
    else
    {
        throw std::runtime_error("boxer: invalid align flags");
    }

    if ((align & HORZ_LEFT) == HORZ_LEFT)
    {
        
    }
    else if ((align & HORZ_RIGHT) == HORZ_RIGHT)
    {
        compx += w-compw;
    }
    else if ((align & HORZ_CENTER) == HORZ_CENTER)
    {
        compx += (w-compw) / 2; /* center */
    }
    else
    {
        throw std::runtime_error("boxer: invalid align flags");
    }

    compx += offsetx;
    compy += offsety;

    component->SetPosition(compx, compy);
}

void TBoxerComponent::Dump(std::ostream& os, unsigned int leadingspace)
{
    // FIXME: I DO NOT CARE BUT THIS IS BROKEN NOW!
    std::string space;
    space.reserve(leadingspace);
    for (int i = 0; i < leadingspace; ++i) space += ' ';
    os << space << "BOXER COMPONENT" << std::endl;
    TBoxerNode::Dump(os, leadingspace);
    /*
    os << space << "compw: " << compw << std::endl;
    os << space << "comph: " << comph << std::endl;
    os << space << "compx: " << compx << std::endl;
    os << space << "compy: " << compy << std::endl;
    */
    os << space << "align: ";
    if (align & VERT_TOP && align & VERT_BOTTOM) os << "VERT_CENTER | ";
    else
    {
        if (align & VERT_TOP) os << "VERT_TOP | ";
        if (align & VERT_BOTTOM) os << "VERT_BOTTOM | ";
    }
    if (align & HORZ_LEFT && align & HORZ_RIGHT) os << "HORZ_CENTER | ";
    else
    {
        if (align & HORZ_LEFT) os << "HORZ_LEFT | ";
        if (align & HORZ_RIGHT) os << "HORZ_RIGHT | ";
    }
    os << std::endl;
}

void TBoxerSpacer::DoBoxing()
{
    // it needs nothing to do
}

void TBoxerSpacer::Dump(std::ostream& os, unsigned int leadingspace)
{
    std::string space;
    space.reserve(leadingspace);
    for (int i = 0; i < leadingspace; ++i) space += ' ';
    os << space << "BOXER SPACER" << std::endl;
    TBoxerNode::Dump(os, leadingspace);
}


void TBoxer::Prepare()
{
    /* max wi and he of minimum sizes of each alternative */
    for (auto &subs: alts)
    {
        for (auto &sub: subs)
        {
            sub->Prepare();
        }
    }
    if (fit)
    {
        double maxw = 0, maxh = 0;
        for (auto &subs: alts)
        {
            double subw = 0, subh = 0;
            for (auto &sub: subs)
            {
                subw += sub->w;
                subh += sub->h;
            }
            if (subw > maxw) maxw = subw;
            if (subh > maxh) maxh = subh;
        }
        w = maxw;
        h = maxh;
    }
}

void TBoxer::DoBoxing()
{
    for (auto &subs: alts)
    {
        double _h, _w, _x, _y;
        _x = x;
        _y = y;
        if (usex)
        {
            _h = xh > 0 ? xh : h;
            _w = xw > 0 ? xw : w;
            if ((xalign & VERT_TOP) == VERT_TOP)
            {
                
            }
            else if ((xalign & VERT_BOTTOM) == VERT_BOTTOM)
            {
                _y += h-_h;
            }
            else if ((xalign & VERT_CENTER) == VERT_CENTER)
            {
                _y += (h-_h) / 2; /* center */
            }
            else
            {
                throw std::runtime_error("boxer: invalid align flags");
            }

            if ((xalign & HORZ_LEFT) == HORZ_LEFT)
            {
                
            }
            else if ((xalign & HORZ_RIGHT) == HORZ_RIGHT)
            {
                _x += w-_w;
            }
            else if ((xalign & HORZ_CENTER) == HORZ_CENTER)
            {
                _x += (w-_w) / 2; /* center */
            }
            else
            {
                throw std::runtime_error("boxer: invalid align flags");
            }
        }
        else
        {
            _h = h; _w = w;
        }
        // will be used for assigning size to proportioned items
        double freespace = (type == TYPE_VERT) ? _h : _w;
        double proptotal = 0;
        double unit;
        double lastx, lasty;
        for (auto &o : subs)
        {
            if (o->prop < 0) // use static sizing
            {
                freespace -= (type == TYPE_VERT) ? o->h : o->w;
            }
            else // use dynamic sizing
            {
                proptotal += o->prop;
            }
        }
        if (freespace < 0)
            throw std::runtime_error("boxer error: not enough space left!");
        unit = freespace / proptotal;
        lastx = _x;
        lasty = _y;
        for (auto &o : subs)
        {
            o->x = lastx;
            o->y = lasty;
            if (o->prop >= 0)
            {
                if (type == TYPE_VERT)
                {
                    o->h = unit * o->prop;
                }
                else
                {
                    o->w = unit * o->prop;
                }
            }
            if (o->expand)
            {
                if (type == TYPE_VERT)
                {
                    o->w = _w;
                }
                else
                {
                    o->h = _h;
                }
            }
            o->DoBoxing();
            if (type == TYPE_VERT)
            {
                lasty += o->h;
            }
            else
            {
                lastx += o->w;
            }
        }
    }
}

void TBoxer::Dump(std::ostream& os, unsigned int leadingspace)
{
#if 0
    // FIXME: I DO NOT CARE BUT THIS IS BROKEN NOW!
    std::string space;
    space.reserve(leadingspace);
    for (int i = 0; i < leadingspace; ++i) space += ' ';
    os << space << "BOXER" << std::endl;
    TBoxerNode::Dump(os, leadingspace);
    os << space << "type: " << ((type == TYPE_HORZ) ? "TYPE_HORZ" : "TYPE_VERT") << std::endl;
    for (auto &o : subs)
    {
        o->Dump(os, leadingspace + 1);
    }
#endif
}

TBoxerNode * TBoxer::FindNodeWithID(const std::string &s)
{
    if (s == id) return this;
    for (auto &subs : alts)
    {
        for (auto &o : subs)
        {
            auto p = dynamic_cast<TBoxer *>(o);
            if (p)
            {
                auto res = p->FindNodeWithID(s);
                if (res)
                {
                    return res;
                }
            }
            else
            {
                if (o->id == s) return o;
            }
        }
    }
    return nullptr;
}

void TBoxer::BindComponent(const std::string &name, TComponent *comp)
{
    auto szrcomp = dynamic_cast<TBoxerComponent *>(FindNodeWithID(name));
    if (szrcomp)
    {
        szrcomp->component = comp;
    }
    else
    {
        throw std::runtime_error("boxer: BindComponent error!");
    }
}

TBoxer::~TBoxer()
{
    for (auto &o : alts)
    {
        for (auto &oo : o) delete oo;
    }
    alts.clear();
}

}
