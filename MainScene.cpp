#include "MainScene.hpp"
#include "Game.hpp"
#include "Exception.hpp"
#include "BackgroundTransition.hpp"
#include "SimpleMenu.hpp"
#include "Button.hpp"
#include "LifeDisplay.hpp"
#include "GameDisplay.hpp"
#include "Random.hpp"
#include "Boxer.hpp"
#include "StaticComponents.hpp"

#include "Server.hpp"

#include <iostream>

namespace BalloonSmashEsque
{

TMainScene::TMainScene(TGame *game)
{
    game_ = game;

    if (game == nullptr) throw TException("Game is nullptr!");

    render_ = game_->GetRenderer();
    win_ = game_->GetWindow();

    current_ = false;
    paused_ = false;
    muted_ = false;

    wi_ = win_->GetWidth();
    he_ = win_->GetHeight();

    // get background transition
    bgtrans_ = game_->GetObj<TBackgroundTransition>("bgtrans");

    // create the menu

    menu_ = std::make_shared<TSimpleMenu>(
        *game_->GetObj<TSimpleMenu>("menubase"));

    (*menu_)
        (1, "OPTS", game_->GetObj<SDL2pp::Texture>("texture.icons.options"))
        (2, "MENU", game_->GetObj<SDL2pp::Texture>("texture.icons.back"))
        (3, "QUIT", game_->GetObj<SDL2pp::Texture>("texture.icons.exit"));

    menu_->SetPosition(SDL2pp::Point{ 10, he_ - menu_->GetHeight() - 10 - 1 });

    menu_->OnClick([this](std::size_t i) {
        if (i == 2) // FIXME must do its real job
        {
            lifedisp_->DieOneMore();
        }
    });

    // create pause and mute buttons

    std::shared_ptr<TButtonFactory> buttonfactory =
        game_->GetObj<TButtonFactory>("smallButtonFactory");

    mute_ = (*buttonfactory)({0, 0},
        game_->GetObj<SDL2pp::Texture>("texture.icons.audioOff"));
    pause_ = (*buttonfactory)({0, 0},
        game_->GetObj<SDL2pp::Texture>("texture.icons.pause"));
    unmute_ = (*buttonfactory)({0, 0},
        game_->GetObj<SDL2pp::Texture>("texture.icons.audioOn"));
    play_ = (*buttonfactory)({0, 0},
        game_->GetObj<SDL2pp::Texture>("texture.icons.play"));

    game_->AddNotification<bool>("muted", [this](const bool &val){
        muted_ = val;
    });

    mute_->OnClick([this]() {
        game_->Set<bool>("muted", true);
    });

    unmute_->OnClick([this]() {
        game_->Set<bool>("muted", false);
    });

    play_->OnClick([this]() {
        game_->Resume();
    });

    pause_->OnClick([this]() {
        game_->Pause();
    });

    // now initialize life display

    lifedisp_ = std::make_shared<TLifeDisplay>(
        render_,
        game_->GetObj<SDL2pp::Font>("font.default"),
        game_->GetObj<SDL2pp::Texture>("texture.heart"),
        SDL2pp::Point { 10, 10 },
        5 /* MAKE THOSE CUSTOMIZABLE */,
        5);

    lifedisp_->SetLifeCount(3);

    // initialize game display
    {
        std::size_t ballcount = game_->Get<int>("int.ballsCount");

        std::vector<std::shared_ptr<SDL2pp::Texture>> balltextures;
        for (int i = 1; i <= ballcount; i++)
        {
            balltextures.push_back(game_->
                GetObj<SDL2pp::Texture>("texture.balls." + std::to_string(i)));
        }

        std::shared_ptr<SDL2pp::Texture> bombtexture;
        bombtexture = game_->GetObj<SDL2pp::Texture>("texture.bomb");

        std::shared_ptr<SDL2pp::Texture> blocktexture;
        blocktexture = game_->GetObj<SDL2pp::Texture>("texture.block");

        SDL2pp::Point sz { 700, 900 };
        SDL2pp::Point pos;
        pos = (SDL2pp::Point { wi_, he_ } - sz) / 2;
        pos.x = 500;

        /* initialize balls */
        std::list<TGameDisplay::ball_t> balls;
        TRandomGenerator rand;
        int j = 10;
        for (int i = 0; i < 10; ++i, --j)
        {
            if (i % 2) // ODD
            {
                balls.push_back({
                    {i, 0},
                    (std::size_t) rand.NewLong(0, ballcount - 1)});
                for (int k = 1; k <= (j-1) / 2; ++k)
                {
                    balls.push_back({
                        {i, +k},
                        (std::size_t) rand.NewLong(0, ballcount - 1)});

                    balls.push_back({
                        {i, -k},
                        (std::size_t) rand.NewLong(0, ballcount - 1)});
                }
            }
            else // EVEN
            {
                for (int k = 1; k <= j / 2; ++k)
                {
                    balls.push_back({
                        {i, +k},
                        (std::size_t) rand.NewLong(0, ballcount - 1)});

                    balls.push_back({
                        {i, -k},
                        (std::size_t) rand.NewLong(0, ballcount - 1)});
                }
            }
        }

        std::list<TGameDisplay::bomb_t> bombs {
            { {200.0, 100.0}, {200.0, 200.0} },
            { {300.0, 100.0}, {-200.0, 200.0} },
            { {400.0, 100.0}, {-150.0, 250.0} }
        };

        gdisp_ = std::make_shared<TGameDisplay>(
            render_,
            balltextures,
            bombtexture,
            blocktexture,
            pos,
            sz,
            30);

        gdisp_->SetBalls(balls);
        gdisp_->SetBombs(bombs);
    }

    // set up display
    status_ = std::make_shared<TStaticText>(
        render_,
        game_->GetObj<SDL2pp::Font>("font.statusText"),
        SDL_Color { 0, 0, 0, 255 },
        "this is the status text",
        SDL2pp::Point { 0, 0 }
    );

    // data recv event
    dataRecvEvent_ = game_->Get<Uint32>("sdl_event.DataRecv");

    boxer_ = game_->GetObj<TBoxer>("boxer.mainscene");
    boxer_->BindComponent("lifeDisplay", lifedisp_.get());
    boxer_->BindComponent("muteButton", mute_.get());
    boxer_->BindComponent("pauseButton", pause_.get());
    boxer_->BindComponent("mainMenu", menu_.get());
    boxer_->BindComponent("gameDisplay", gdisp_.get());
    boxer_->BindComponent("statusDisplay", status_.get());

    boxer_->x = 0;
    boxer_->y = 0;
    boxer_->w = win_->GetWidth();
    boxer_->h = win_->GetHeight();

    boxer_->Prepare();
    boxer_->DoBoxing();

    unmute_->SetPosition(mute_->GetPosition());
    play_->SetPosition(pause_->GetPosition());
}

TMainScene::~TMainScene()
{
}

void TMainScene::Render()
{
    bgtrans_->Render();
    menu_->Render();

    if (muted_)
    {
        unmute_->Render();
    }
    else
    {
        mute_->Render();
    }

    if (paused_)
    {
        play_->Render();
    }
    else
    {
        pause_->Render();
    }

    lifedisp_->Render();

    gdisp_->Render();
    status_->Render();

    if (!paused_) gdisp_->NextStep();
}

void TMainScene::Handle(SDL_Event& e)
{
    if (e.type == SDL_QUIT)
    {
        game_->Stop();
        return ;
    }
    else if (e.type == dataRecvEvent_)
    {
        int *data = (int *) e.user.data1;
        gdisp_->SetBlockPosition((*data - 512) / 512.0 * gdisp_->GetBlockMaxPos());
        delete data;
        return ;
    }

    if (!paused_)
    {
        menu_->Handle(e);

        if (muted_)
        {
            unmute_->Handle(e);
        }
        else
        {
            mute_->Handle(e);
        }

        if (paused_)
        {
            play_->Handle(e);
        }
        else
        {
            pause_->Handle(e);
        }

        gdisp_->Handle(e);
    }
    else
    {
        
    }
}

void TMainScene::OnPause()
{
    paused_ = true;
}

void TMainScene::OnResume()
{
    paused_ = false;
    gdisp_->ResetTimer();
}

void TMainScene::OnCurrent()
{
    current_ = true;
    gdisp_->ResetTimer();
}

void TMainScene::OnNotCurrent()
{
    current_ = false;
}

}
