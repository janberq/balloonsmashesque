#ifndef _DIALOG_HPP_INCLUDED
#define _DIALOG_HPP_INCLUDED

#include <SDL2/SDL.h>

namespace BalloonSmashEsque
{

class TGame;

class TDialog
{

public:

    virtual void Render();
    virtual void Handle(SDL_Event &e);

    virtual void OnShown();
    virtual void OnGone();

    virtual SDL_Color GetShadeColor() const;

    virtual ~TDialog();

};

}

#endif // _DIALOG_HPP_INCLUDED
