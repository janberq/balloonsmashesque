#include "Button.hpp"

namespace BalloonSmashEsque
{

TButton::TButton(
    std::shared_ptr<SDL2pp::Renderer> renderer,
    std::shared_ptr<SDL2pp::Texture> normal,
    const SDL2pp::Point& pos,
    std::shared_ptr<SDL2pp::Texture> hover,
    std::shared_ptr<SDL2pp::Texture> pressed,
    std::shared_ptr<SDL2pp::Texture> image)
{
    state_ = STATE_NORMAL;
    renderer_ = renderer;
    pos_ = pos;

    wi_ = normal->GetWidth();
    he_ = normal->GetHeight();

    if (!hover) hover = normal;
    if (!pressed) pressed = normal;

    if (!image)
    {
        normal_ = normal;
        hover_ = hover;
        pressed_ = pressed;
    }
    else
    {
        int imwi = image->GetWidth();
        int imhe = image->GetHeight();
        SDL2pp::Rect dst {
            (wi_ - imwi) / 2,
            (he_ - imhe) / 2,
            imwi,
            imhe
        };

        normal_ = std::make_shared<SDL2pp::Texture>(
            *renderer_, SDL_PIXELFORMAT_RGBA8888,
            SDL_TEXTUREACCESS_TARGET,
            wi_, he_);
        normal_->SetBlendMode(SDL_BLENDMODE_BLEND);
        renderer_->SetTarget(*normal_);
        renderer_->SetDrawBlendMode(SDL_BLENDMODE_BLEND);
        renderer_->Clear();
        renderer_->Copy(*normal, SDL2pp::NullOpt, SDL2pp::NullOpt);
        renderer_->Copy(*image, SDL2pp::NullOpt, dst);

        hover_ = std::make_shared<SDL2pp::Texture>(
            *renderer_, SDL_PIXELFORMAT_RGBA8888,
            SDL_TEXTUREACCESS_TARGET,
            wi_, he_);
        hover_->SetBlendMode(SDL_BLENDMODE_BLEND);
        renderer_->SetTarget(*hover_);
        renderer_->SetDrawBlendMode(SDL_BLENDMODE_BLEND);
        renderer_->Clear();
        renderer_->Copy(*hover, SDL2pp::NullOpt, SDL2pp::NullOpt);
        renderer_->Copy(*image, SDL2pp::NullOpt, dst);

        pressed_ = std::make_shared<SDL2pp::Texture>(
            *renderer_, SDL_PIXELFORMAT_RGBA8888,
            SDL_TEXTUREACCESS_TARGET,
            wi_, he_);
        pressed_->SetBlendMode(SDL_BLENDMODE_BLEND);
        renderer_->SetTarget(*pressed_);
        renderer_->SetDrawBlendMode(SDL_BLENDMODE_BLEND);
        renderer_->Clear();
        renderer_->Copy(*pressed, SDL2pp::NullOpt, SDL2pp::NullOpt);
        renderer_->Copy(*image, SDL2pp::NullOpt, dst);

        renderer_->SetTarget();
    }

}

TButton::~TButton()
{
}

void TButton::Render()
{
    switch (state_)
    {
        case STATE_HOVER:
            renderer_->Copy(*hover_, SDL2pp::NullOpt,
                SDL2pp::Rect{ pos_.x, pos_.y, wi_, he_ });
            break;
        case STATE_NORMAL:
            renderer_->Copy(*normal_, SDL2pp::NullOpt,
                SDL2pp::Rect{ pos_.x, pos_.y, wi_, he_ });
            break;
        case STATE_PRESSED:
            renderer_->Copy(*pressed_, SDL2pp::NullOpt,
                SDL2pp::Rect{ pos_.x, pos_.y, wi_, he_ });
            break;
    }
}

void TButton::Handle(SDL_Event& e)
{
    auto inside = [this](int x, int y) -> bool {
        return x >= pos_.x && x < pos_.x + wi_ &&
            y >= pos_.y && y < pos_.y + he_;
    };
    if (e.type == SDL_MOUSEMOTION)
    {
        if (state_ != STATE_PRESSED)
        {
            if(inside(e.motion.x, e.motion.y))
            {
                state_ = STATE_HOVER;
            }
            else
            {
                state_ = STATE_NORMAL;
            }
        }
    }
    else if (e.type == SDL_MOUSEBUTTONDOWN)
    {
        if (inside(e.button.x, e.button.y))
        {
            state_ = STATE_PRESSED;
        }
    }
    else if (e.type == SDL_MOUSEBUTTONUP)
    {
        if (inside(e.button.x, e.button.y))
        {
            if (onclick_) onclick_();
        }
        state_ = STATE_NORMAL;
    }
}

int TButton::GetWidth() const
{
    return wi_;
}

int TButton::GetHeight() const
{
    return he_;
}

void TButton::OnClick(std::function<void ()> onclick)
{
    onclick_ = onclick;
}


TButtonFactory::TButtonFactory(
    std::shared_ptr<SDL2pp::Renderer> renderer,
    std::shared_ptr<SDL2pp::Texture> normal,
    std::shared_ptr<SDL2pp::Texture> hover,
    std::shared_ptr<SDL2pp::Texture> pressed)
{
    renderer_ = renderer;
    normal_ = normal;
    hover_ = hover;
    pressed_ = pressed;
}

std::shared_ptr<TButton> TButtonFactory::operator()(
    const SDL2pp::Point& pos,
    std::shared_ptr<SDL2pp::Texture> img)
{
    return std::make_shared<TButton>(
        renderer_,
        normal_,
        pos,
        hover_,
        pressed_,
        img);
}

int TButtonFactory::GetButtonWidth() const
{
    return normal_->GetWidth();
}

int TButtonFactory::GetButtonHeight() const
{
    return normal_->GetHeight();
}

}
