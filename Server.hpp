#ifndef _SERVER_HPP_INCLUDED
#define _SERVER_HPP_INCLUDED

#include <memory>
#include <string>
#include <functional>

namespace BalloonSmashEsque
{

class TServerImpl;

class TServer
{
public:

    TServer(unsigned short portnum, bool autorelisten_ = true);

    TServer(const TServer &) = delete;
    TServer &operator=(const TServer &) = delete;

    TServer(TServer &&);
    TServer &operator=(TServer &&);

    void OnDataRecv(std::function<void (const std::string &)> func);
    void OnLog(std::function<void (const std::string &)> func);

    void Start();

    bool IsListening() const;
    bool IsConnected() const;
    bool IsOK() const;

    ~TServer();

protected:
    std::unique_ptr<TServerImpl> impl_;
};

}

#endif // _SERVER_HPP_INCLUDED
