#include "Scene.hpp"

namespace BalloonSmashEsque
{

void TScene::Render()
{
}

void TScene::Handle(SDL_Event& e)
{
}

void TScene::OnCurrent()
{
}

void TScene::OnNotCurrent()
{
}

void TScene::OnPause()
{
}

void TScene::OnResume()
{
}

TScene::~TScene()
{
}

}
