#include "LifeDisplay.hpp"

namespace BalloonSmashEsque
{

TLifeDisplay::TLifeDisplay(
    std::shared_ptr<SDL2pp::Renderer> renderer,
    std::shared_ptr<SDL2pp::Font> font,
    std::shared_ptr<SDL2pp::Texture> hearttexture,
    const SDL2pp::Point &pos,
    int heartseparation,
    int textheartseparation)
{
    renderer_ = renderer;
    heart_ = hearttexture;
    pos_ = pos;
    heartseparation_ = heartseparation;
    textheartseparation_ = textheartseparation;

    text_ = std::make_shared<SDL2pp::Texture>(
        *renderer_,
        font->RenderUTF8_Blended("LEFT:",
        SDL_Color {0, 0, 0, 255} /* TODO MAKE THIS CUSTOMIZABLE */));

}

int TLifeDisplay::GetLifeCount() const
{
    return lifecount_;
}

void TLifeDisplay::SetLifeCount(int n)
{
    lifecount_ = n;
}

void TLifeDisplay::DieOneMore()
{
    if (lifecount_ > 0) lifecount_--;
}

void TLifeDisplay::DieFull()
{
    lifecount_ = 0;
}

int TLifeDisplay::GetWidth() const
{
    int wi1 = text_->GetWidth();
    int wi2 = lifecount_ * heart_->GetWidth() +
        (lifecount_ - 1) * heartseparation_;
    return (wi1 > wi2) ? wi1 : wi2;
}

int TLifeDisplay::GetHeight() const
{
    return text_->GetHeight() + textheartseparation_ + heart_->GetHeight();
}

void TLifeDisplay::Render()
{
    renderer_->Copy(
        *text_, SDL2pp::NullOpt,
        SDL2pp::Rect(
            pos_.x,
            pos_.y,
            text_->GetWidth(),
            text_->GetHeight()));

    SDL2pp::Rect dst;
    dst.x = pos_.x;
    dst.y = pos_.y + text_->GetHeight() + textheartseparation_;
    dst.w = heart_->GetWidth();
    dst.h = heart_->GetHeight();

    for (int i = 0; i < lifecount_; ++i)
    {
        renderer_->Copy(*heart_, SDL2pp::NullOpt, dst);
        dst.x += dst.w + heartseparation_;
    }
}

}
