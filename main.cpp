#include <iostream>

#include "Game.hpp"

#include <SDL2pp/SDL2pp.hh>
#include <exception>

int main(int argc, char **argv) {
    // try
    {
        using namespace SDL2pp;
        using namespace BalloonSmashEsque;

        SDL sdl(SDL_INIT_VIDEO);
        SDLTTF sdlttf;
        SDLImage sdlimage;

        TGame game(argc, argv);
        return game.Run();
    }
    /* catch (std::exception &ex)
    {
        std::cerr << "Exception: " << ex.what() << std::endl;
        return 1;
    } */
}
