#ifndef _GAME_HPP_INCLUDED
#define _GAME_HPP_INCLUDED

#include <map>
#include <string>
#include <memory>
#include <list>
#include <functional>

#include <boost/any.hpp>

#include <SDL2pp/SDL2pp.hh>

#include "Exception.hpp"
#include "Context.hpp"
#include "Scene.hpp"
#include "Dialog.hpp"

namespace BalloonSmashEsque
{

class TScene;
class TDialog;

class TGame : public TContext
{

public:

    // Constructors and Destructors

    TGame(int argc, char ** argv);

    TGame(const TGame &) = delete;
    TGame &operator=(const TGame &) = delete;

    TGame(TGame &&) = delete;
    TGame &operator=(TGame &&) = delete;

    ~TGame();

    // for filesystem

    std::string GetResourcePath(const std::string &path) const;

    // Scene control

    void AddScene(const std::string &name, TScene *scene);
    void SetCurrentScene(const std::string &scene);

    // Dialog control

    void SetCurrentDialog(TDialog *dlg);

    // Renderer and Window

    std::shared_ptr<SDL2pp::Renderer> GetRenderer();
    std::shared_ptr<SDL2pp::Window> GetWindow();

    // Control functions

    int Run();
    void Pause();
    void Resume();
    void Stop();

    bool IsRunning() const;
    bool IsPaused() const;

protected:

    std::shared_ptr<SDL2pp::Window> window_;
    std::shared_ptr<SDL2pp::Renderer> renderer_;
    std::map<std::string, std::shared_ptr<SDL2pp::Texture>> textures_;
    std::map<std::string, TScene *> scenes_;
    TScene *current_scene_ { nullptr };
    TDialog *current_dialog_ { nullptr };

    bool continue_;
    bool paused_;
    bool running_;
    bool norun_;

    int exit_code_;

    std::string datadir_;

private:

    void Init(int argc, char **argv);
    void Deinit();

};

}

#endif // _GAME_HPP_INCLUDED
