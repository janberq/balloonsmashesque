#ifndef _BACKGROUNDTRANSITION_HPP_INCLUDED
#define _BACKGROUNDTRANSITION_HPP_INCLUDED

#include <SDL2pp/SDL2pp.hh>
#include <memory>
#include <vector>
#include <functional>

namespace BalloonSmashEsque
{

class TBackgroundTransition
{
public:

    using TTransitionFunction = std::function<bool (int, double &)>;

    TBackgroundTransition(
        std::shared_ptr<SDL2pp::Renderer> renderer,
        const SDL2pp::Rect &dest);

    /* use this to create transition */
    TBackgroundTransition &operator()(
        std::shared_ptr<SDL2pp::Texture> texture,
        size_t duration,
        TTransitionFunction transition_function);

    void Start(std::size_t start_texture = 0);

    void Render();

    static TTransitionFunction linearTransition(
        std::size_t maintransitionduration,
        std::size_t previousdurationfirst = 0,
        std::size_t previousdurationsecond = 0,
        double previousproportion = 0,
        std::size_t nextdurationfirst = 0,
        std::size_t nextdurationsecond = 0,
        double nextproportion = 0);

    static TTransitionFunction logarithmicTransition(
        std::size_t maintransitionduration);

private:

    /* show durations */
    std::vector<std::size_t> durations_;

    /* textures */
    std::vector<std::shared_ptr<SDL2pp::Texture>> textures_;

    /* texture sizes */
    std::vector<SDL2pp::Point> sizes_;

    /* transition functions */
    std::vector<TTransitionFunction> transition_functions_;

    /* renderer */
    std::shared_ptr<SDL2pp::Renderer> renderer_;

    /* destination */
    SDL2pp::Rect dest_;

    /* current texture */
    std::size_t current_;

    /* transition proportion */
    double proportion_;

    /* time since the current texture is fully shown,
     * or the transition is started */
    std::size_t time_;

    /* is in transition */
    bool transition_;

};

}

#endif // _BACKGROUNDTRANSITION_HPP_INCLUDED
