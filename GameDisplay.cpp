#include "GameDisplay.hpp"
#include <cmath>
#include <algorithm>

namespace BalloonSmashEsque
{

TGameDisplay::TGameDisplay(
    std::shared_ptr<SDL2pp::Renderer> renderer,
    const std::vector<std::shared_ptr<SDL2pp::Texture>> &balltextures,
    std::shared_ptr<SDL2pp::Texture> bombtexture,
    std::shared_ptr<SDL2pp::Texture> blocktexture,
    const SDL2pp::Point &pos,
    const SDL2pp::Point &sz,
    double hexgridrad,
    const SDL_Color &borderclr)
{
    renderer_ = renderer;
    balltextures_ = balltextures;
    bombtexture_ = bombtexture;
    blocktexture_ = blocktexture;
    pos_ = pos;
    sz_ = sz;
    hexgridrad_ = hexgridrad;
    borderclr_ = borderclr;

    blockcoord_ = 0;

    ballrad_ = balltextures_[0]->GetWidth() / 2.0;
    bombrad_ = bombtexture_->GetWidth() / 2.0;
    blockhe_ = blocktexture_->GetHeight();
    blockwi_ = blocktexture_->GetWidth();

    keyboardenabled_ = true;
    keyboardspeed_ = 300;
}

void TGameDisplay::Handle(SDL_Event &e)
{
    if (keyboardenabled_)
    {
        if (e.type == SDL_KEYDOWN)
        {
            pressedkeys_.insert({
                e.key.keysym.sym,
                SDL_GetTicks() });
        }
        else if (e.type == SDL_KEYUP)
        {
            auto it = GetKey(e.key.keysym.sym);
            if (it != pressedkeys_.end())
            {
                pressedkeys_.erase(it);
            }
        }
    }
}

void TGameDisplay::Render()
{
    RenderBorder();
    RenderBombs();
    RenderBalls();
    RenderBlock();
}

void TGameDisplay::SetBalls(const std::list<ball_t> &balls)
{
    balls_ = balls;
}

void TGameDisplay::SetBombs(const std::list<bomb_t> &bombs)
{
    bombs_ = bombs;
}

void TGameDisplay::AddBall(const ball_t &ball)
{
    balls_.push_back(ball);
}

void TGameDisplay::AddBomb(const bomb_t &bomb)
{
    bombs_.push_back(bomb);
}

void TGameDisplay::SetBlockPosition(double pos)
{
    blockcoord_ = pos;
    if (blockcoord_ > (sz_.x - blockwi_) / 2.0)
    {
        blockcoord_ = (sz_.x - blockwi_) / 2.0;
    }
    else if (blockcoord_ < -(sz_.x - blockwi_) / 2.0)
    {
        blockcoord_ = -(sz_.x - blockwi_) / 2.0;
    }
}

double TGameDisplay::GetBlockMaxPos() const
{
    return (sz_.x - blockwi_) / 2.0;
}

double TGameDisplay::GetBlockMinPos() const
{
    return -(sz_.x - blockwi_) / 2.0;
}

double TGameDisplay::GetBlockWidth() const
{
    return blockwi_;
}

SDL2pp::Point TGameDisplay::GetBlockSize() const
{
    return { (int) blockwi_, (int) blockhe_ };
}

void TGameDisplay::SetWidth(int wi)
{
    sz_.x = wi;
}

void TGameDisplay::SetHeight(int he)
{
    sz_.y = he;
}

void TGameDisplay::SetSize(const SDL2pp::Point &sz)
{
    sz_ = sz;
}

int TGameDisplay::GetWidth() const
{
    return sz_.x;
}

int TGameDisplay::GetHeight() const
{
    return sz_.y;
}

bool TGameDisplay::IsKeyboardEnabled() const
{
    return keyboardenabled_;
}

void TGameDisplay::SetKeyboardEnable(bool v)
{
    keyboardenabled_ = v;
}

void TGameDisplay::OnBallsGone(std::function<void ()> f)
{
    onballsgone_ = f;
}

void TGameDisplay::OnAllBallsGone(std::function<void ()> f)
{
    onallballsgone_ = f;
}

void TGameDisplay::OnBombFallen(std::function<void (const bomb_t&)> f)
{
    onbombfallen_ = f;
}

TPoint<double> TGameDisplay::ConvHexToCart(
    const TPoint<int> &hex) const
{
    static double r_root_3 = hexgridrad_ * std::sqrt(3);
    TPoint<double> p;
    if (hex.x % 2) // ODD
    {
        p = TPoint<double> (
            hex.y * r_root_3,
            hexgridrad_ * (2.5 + 1.5 * (double)(hex.x - 1)));
    }
    else // EVEN
    {
        if (hex.y > 0)
        {
            p = TPoint<double> (
                r_root_3 * (0.5 + (double)(hex.y - 1)),
                hexgridrad_ * (1.0 + 1.5 * hex.x));
        }
        else
        {
            p = TPoint<double> (
                - r_root_3 * (0.5 + (double)(-hex.y - 1)),
                hexgridrad_ * (1.0 + 1.5 * hex.x));
        }
    }
    p.y = sz_.y - p.y - 1;
    return p;
}

void TGameDisplay::ResetTimer()
{
    tprevious_ = SDL_GetTicks();
}

void TGameDisplay::NextStep()
{
    std::size_t now = SDL_GetTicks();
    std::set<key_t>::iterator key_it;
    if ((key_it = GetKey(SDLK_RIGHT)) != pressedkeys_.end())
    {
        blockcoord_ += (now-key_it->t) * keyboardspeed_ / 1000.0;
        key_it->t = now;
    }
    else if ((key_it = GetKey(SDLK_LEFT)) != pressedkeys_.end())
    {
        blockcoord_ -= (now-key_it->t) * keyboardspeed_ / 1000.0;
        key_it->t = now;
    }
    SetBlockPosition(blockcoord_); // ensure the limits

    Motion();
    HandleCollisions();
}

SDL2pp::Point TGameDisplay::ConvCartToScr(
    const TPoint<double> &cart) const
{
    return SDL2pp::Point {
        (int) (pos_.x + sz_.x / 2.0 + cart.x),
        (int) (pos_.y + sz_.y - cart.y - 1)
    };
}

void TGameDisplay::RenderCentered(
    std::shared_ptr<SDL2pp::Texture> texture,
    const TPoint<double> &pos)
{
    auto pos_scr = ConvCartToScr(pos);
    SDL2pp::Rect dst;
    dst.w = texture->GetWidth();
    dst.h = texture->GetHeight();
    dst.x = pos_scr.x - dst.w / 2.0;
    dst.y = pos_scr.y - dst.h / 2.0;
    renderer_->Copy(*texture, SDL2pp::NullOpt, dst);
}

void TGameDisplay::RenderBombs()
{
    for (auto &o : bombs_)
    {
        RenderCentered(bombtexture_, o.pos);
    }
}

void TGameDisplay::RenderBalls()
{
    for (auto &b : balls_)
    {
        RenderCentered(balltextures_[b.color], ConvHexToCart(b.coords));
    }
}

void TGameDisplay::RenderBlock()
{
    auto coords = ConvCartToScr({ (double) blockcoord_, 0 });
    renderer_->Copy(*blocktexture_,
        SDL2pp::NullOpt,
        coords - SDL2pp::Point(blocktexture_->GetWidth() / 2,
                               blocktexture_->GetHeight()));
}

void TGameDisplay::RenderBorder()
{
    renderer_->SetDrawColor(
        borderclr_.r,
        borderclr_.g,
        borderclr_.b,
        borderclr_.a);
    renderer_->DrawRect(SDL2pp::Rect(pos_.x, pos_.y, sz_.x, sz_.y));
}

void TGameDisplay::Motion()
{
    std::size_t now = SDL_GetTicks();
    for (auto &b : bombs_)
    {
        b.pos += b.velocity * (now - tprevious_) / 1000.0;
    }
    tprevious_ = now;
}

void TGameDisplay::HandleCollisions()
{
    auto horzcoll = [this](bomb_t &b) {
        b.velocity.y *= -1.0;
    }; // horizontal collision
    std::list<std::list<bomb_t>::iterator> bombstoremove;
    for (auto bombit = bombs_.begin(); bombit != bombs_.end(); bombit++)
    {
        if (bombit->pos.y <= bombrad_)
        {
            // collision with the ground
            if (onbombfallen_) onbombfallen_(*bombit);
            bombstoremove.push_back(bombit);
        }
        else if (
            bombit->pos.y < bombrad_ + blockhe_ &&
            std::abs(bombit->pos.x - blockcoord_) < blockwi_ / 2.0)
        {
            // collsion with the block
            bombit->pos.y = bombrad_ + blockhe_;
            horzcoll(*bombit);
        }
        else if (
            bombit->pos.x < (-sz_.x/2.0 + bombrad_) ||
            bombit->pos.x > (sz_.x/2.0 - bombrad_))
        {
            if (bombit->pos.x <= (-sz_.x/2.0 + bombrad_))
            {
                bombit->pos.x = -sz_.x/2.0 + bombrad_;
            }
            else
            {
                bombit->pos.x = (sz_.x/2.0 - bombrad_);
            }
            bombit->velocity.x *= -1.0;
        }
        else if (bombit->pos.y > sz_.y - bombrad_)
        {
            bombit->pos.y = sz_.y - bombrad_;
            horzcoll(*bombit);
        }
        else
        {
            // check collision with balls:
            for (auto &ball : balls_)
            {
                auto ballpos = ConvHexToCart(ball.coords);
                if ((ballpos - bombit->pos).dist() <= ballrad_ + bombrad_)
                {
                    // collision with a ball
                    // calculate new velocity
                    auto prl    = (bombit->pos - ballpos).unit();
                    auto prp    = prl.perp();
                    auto prlcmp = bombit->velocity * prl;
                    auto prpcmp = bombit->velocity * prp;
                    bombit->velocity  = prl * (-prlcmp) + prp * (prpcmp);
                    BreakBallsAt(ball.coords);
                    if (onballsgone_) onballsgone_();
                    if (balls_.empty())
                    {
                        if (onallballsgone_) onallballsgone_();
                    }
                    break;
                }
            }
        }
    }
    for (auto &it : bombstoremove)
    {
        bombs_.erase(it);
    }
}

void TGameDisplay::BreakBallsAt(const TPoint<int> &pos)
{
    using it_t = std::list<ball_t>::iterator;
    std::list<it_t> tobreak;
    std::function<void (it_t &)> addneighbors =
        [&tobreak, this, &addneighbors](it_t &it) -> void {
            if (std::find(tobreak.begin(), tobreak.end(), it) != tobreak.end())
                return ;
            tobreak.push_back(it);
            std::vector<TPoint<int>> neighbors;
            if (it->coords.x % 2) // odd
            {
                if (it->coords.y < 0)
                {
                    neighbors = std::vector<TPoint<int>>{
                        { it->coords.x - 1, it->coords.y },
                        { it->coords.x - 1, it->coords.y - 1 },
                        { it->coords.x, it->coords.y + 1 },
                        { it->coords.x, it->coords.y - 1 },
                        { it->coords.x + 1, it->coords.y },
                        { it->coords.x + 1, it->coords.y - 1 } };
                }
                else if (it->coords.y > 0)
                {
                    neighbors = std::vector<TPoint<int>>{
                        { it->coords.x - 1, it->coords.y },
                        { it->coords.x - 1, it->coords.y + 1 },
                        { it->coords.x, it->coords.y + 1 },
                        { it->coords.x, it->coords.y - 1 },
                        { it->coords.x + 1, it->coords.y },
                        { it->coords.x + 1, it->coords.y + 1 } };
                }
                else
                {
                    neighbors = std::vector<TPoint<int>>{
                        { it->coords.x - 1, 1 },
                        { it->coords.x - 1, -1 },
                        { it->coords.x, it->coords.y + 1 },
                        { it->coords.x, it->coords.y - 1 },
                        { it->coords.x + 1, 1 },
                        { it->coords.x + 1, -1 } };
                }
            }
            else // even
            {
                if (it->coords.y == -1)
                {
                    neighbors = std::vector<TPoint<int>>{
                        { it->coords.x - 1, -1 },
                        { it->coords.x - 1, 0 },
                        { it->coords.x, 1 },
                        { it->coords.x, -2 },
                        { it->coords.x + 1, -1 },
                        { it->coords.x + 1, 0 } };
                }
                else if (it->coords.y == 1)
                {
                    neighbors = std::vector<TPoint<int>>{
                        { it->coords.x - 1, 0 },
                        { it->coords.x - 1, 1 },
                        { it->coords.x, 2 },
                        { it->coords.x, -1 },
                        { it->coords.x + 1, 0 },
                        { it->coords.x + 1, 1 } };
                }
                else
                {
                    neighbors = std::vector<TPoint<int>>{
                        { it->coords.x - 1, it->coords.y },
                        { it->coords.x - 1, it->coords.y + 1 },
                        { it->coords.x, it->coords.y + 1 },
                        { it->coords.x, it->coords.y - 1 },
                        { it->coords.x + 1, it->coords.y },
                        { it->coords.x + 1, it->coords.y + 1 } };
                }
            }
            for (auto &coords : neighbors)
            {
                for (auto it2 = balls_.begin(); it2 != balls_.end(); ++it2)
                {
                    if (it2->coords == coords && it2->color == it->color)
                    {
                        addneighbors(it2);
                    }
                }
            }
        };
    for (auto it = balls_.begin(); it != balls_.end(); ++it)
    {
        if (it->coords == pos)
        {
            addneighbors(it);
            break;
        }
    }
    for (auto &it : tobreak)
    {
        balls_.erase(it);
    }
}

std::set<TGameDisplay::key_t>::iterator TGameDisplay::GetKey(SDL_Keycode code)
{
    return std::find_if(
        pressedkeys_.begin(),
        pressedkeys_.end(),
        [code](const key_t &k)
        {
            return (code == k.code);
        });
}

}
