#ifndef _POINT_HPP_INCLUDED
#define _POINT_HPP_INCLUDED

#include <SDL2pp/Point.hh>
#include <cmath>

namespace BalloonSmashEsque
{

template <typename T>
class TPoint
{

public:

    T x, y;

    constexpr TPoint() : x(0), y(0) {  }
    constexpr TPoint(T x_, T y_) : x(x_), y(y_) {  }
    constexpr TPoint(const TPoint &p) : x(p.x), y(p.y) {  }
    constexpr TPoint(const SDL2pp::Point &p) : x(p.x), y(p.y) {  }
    constexpr TPoint(const SDL_Point &p) : x(p.x), y(p.y) {  }

    constexpr operator SDL2pp::Point() const { return SDL2pp::Point(x, y); }
    constexpr operator SDL_Point() const { return SDL_Point(x, y); }

    TPoint operator+(const TPoint &p) const
    { return TPoint { x + p.x, y + p.y }; }

    TPoint operator-(const TPoint &p) const
    { return TPoint { x - p.x, y - p.y }; }

    TPoint operator*(double s) const
    { return TPoint { x * s, y * s }; }

    double operator*(const TPoint &p) const
    { return x * p.x + y * p.y; }

    TPoint operator/(double s) const
    { return TPoint { x / s, y / s }; }

    TPoint &operator+=(const TPoint &p)
    { *this = *this + p; }

    TPoint &operator-=(const TPoint &p)
    { *this = *this - p; }

    TPoint &operator*=(double s)
    { *this = *this * s; }

    TPoint &operator/=(double s)
    { *this = *this / s; }

    bool operator==(const TPoint &p) const
    { return x == p.x && y == p.y; }

    bool operator!=(const TPoint &p) const
    { return !(*this == p); }

    double dist() const
    { return std::sqrt(x * x + y * y); }

    double norm() const
    { return dist(); }

    TPoint unit() const
    { return *this / norm(); }

    TPoint perp() const
    { return TPoint { -y, x }; }

};

}

#endif
